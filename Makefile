LANDO 		= @lando
COMPOSER 	= $(LANDO) composer
PHP 			= $(LANDO) php
SYMFONY 	= $(PHP) bin/console
YARN 			= @yarn


## —— 🔥 App —————————————————————————————————————————————————————————————————
init: ## Init the project
	$(MAKE) start-containers
	$(MAKE) install-vendor
	$(MAKE) install-nm
	$(MAKE) seed
	$(MAKE) start
	
start: ## Start the app
	$(MAKE) start-containers
	$(YARN) watch

rebuild: ## Rebuild the app
	$(LANDO) rebuild -y
	$(MAKE) clean-vendor
	$(MAKE) seed
	$(MAKE) start

update: ## Update the app
	$(COMPOSER) update
	$(YARN) upgrade

cc: ## Clear the app's cache
	$(MAKE) cc-symfony
	$(COMPOSER) cc
	$(YARN) cache clean
	$(LANDO) --clear

.PHONY: tests
tests: ## Run all tests
	$(SYMFONY) d:d:d --force --if-exists --env=test
	$(SYMFONY) d:d:c --env=test
	$(SYMFONY) d:m:m --no-interaction --env=test
	$(SYMFONY) d:f:l --no-interaction --env=test
	$(PHP) bin/phpunit --testdox tests/Unit/
	$(PHP) bin/phpunit --testdox tests/Functional/


stop: ## Stop the app
	$(LANDO) stop


## —— 🐳 Lando ———————————————————————————————————————————————————————————————
start-containers: ## Starts all containers
	$(LANDO) start


## —— 🎻 Composer —————————————————————————————————————————————————————————————
install-vendor: ## Installs all vendor dependencies
	$(COMPOSER) install --prefer-dist

clean-vendor: ## Resets the vendor folder
	$(MAKE) cc
	rm -Rf vendor
	rm composer.lock
	$(COMPOSER) install


## —— 🐈 Yarn —————————————————————————————————————————————————————————————————
install-nm: ## Installs all yarn dependencies
	$(YARN) install --force


## —— 🎶 Symfony ——————————————————————————————————————————————————————————————
set-db: ## Set database
	$(SYMFONY) d:d:d --force
	$(SYMFONY) d:d:c
	$(SYMFONY) d:m:m --no-interaction

seed: ## Reinit database
	$(MAKE) set-db
	$(SYMFONY) d:f:l --no-interaction

cc-symfony: ## Delete cache
	rm -Rf var/cache/*

entity: ## Set entity
	$(SYMFONY) make:entity

migration: ## Set migration
	$(SYMFONY) make:migration 

controller: ## Set controller
	$(SYMFONY) make:controller 

## —— 🛠️  Others ————————————————————————————————————————————————————————————————
help: ## List of commands
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
