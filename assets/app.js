/* External dependencies */
import 'slick-carousel';

/* CSS files */
import './styles/app.scss';

/* JS classes */
import Header from './scripts/_header';
import Opinions from './scripts/_opinions';
import InformationBanner from './scripts/_information-banner';

/* Header */
const primaryHeader = document.querySelector('.header__primary-mobile');
const menuIcon = document.querySelector('.menu-icon');
const menuIconItems = [].slice.call(document.querySelectorAll('.menu-icon__item'));

if (primaryHeader && menuIcon && menuIconItems) {
  new Header(primaryHeader, menuIcon, menuIconItems);
}

/* Opinions */
const opinions = document.querySelector('.opinions');

if (opinions) {
  new Opinions(opinions);
}

/**
 * Information Banner
 */
const informationBanner = document.querySelector('#information-banner');
if (informationBanner) {
  new InformationBanner(informationBanner);
}



