export default class Navbar {
  /**
   * 
   * @param {HTMLElement} primaryHeader 
   * @param {HTMLELement} menuIcon 
   * @param {Array<HTMLElement>} menuIconItems 
   */
  constructor(primaryHeader, menuIcon, menuIconItems) {
    this.primaryHeader = primaryHeader;
    this.menuIcon = menuIcon;
    this.menuIconItems = menuIconItems;

    if (this.primaryHeader && this.menuIcon && menuIconItems) {
      this.listenOnClickMenuIcon();
    }
  }

  /**
   * Listening to the click on the icon menu
   */
  listenOnClickMenuIcon() {
    this.menuIcon.addEventListener('click', () => {
      this.toggleElement(this.primaryHeader, 'none');

      this.menuIconItems.map(item => {
        this.toggleElement(item, 'change');
      });
    })
  }

  /**
   * Toggle a choosen class on a choosen element
   * 
   * @param {HTMLElement} elt 
   * @param {String} className 
   */
  toggleElement(elt, className) {
    elt.classList.toggle(className);
  }
}

