export default class InformationBanner {
  constructor(informationBanner) {
    this.informationBanner = informationBanner;
    this.localStorageValue = localStorage.getItem('information-banner-pefr');
    console.log(this.localStorageValue)

    if (this.informationBanner && this.localStorageValue !== 'hide') {
      this.closeBtn = this.informationBanner.querySelector('.information-banner__close');

      if (this.localStorageValue === null) {
        localStorage.setItem('information-banner-pefr', 'active');
      }

      this.informationBanner.classList.add('information-banner--active');

      this.closeBtn.addEventListener('click', (event) => {
        event.preventDefault();
        this.hideBanner();
      })
    }
  }

  hideBanner() {
    this.informationBanner.classList.remove('information-banner--active');
    localStorage.setItem('information-banner-pefr', 'hide');
  }
}