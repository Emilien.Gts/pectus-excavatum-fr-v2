import $ from 'jquery';

export default class Opinions {
  /**
   * 
   * @param {Array<HTMLElement>} opinions 
   */
  constructor(opinions) {
    this.opinions = opinions;

    if (this.opinions) {
      this.initCarousel(this.opinions);
    }
  }

  /**
   * Init carousel with slick
   * 
   * @param {Array<HTMLElement>} opinions 
   */
  initCarousel(opinions) {
    $(opinions).slick({
      dots: true,
      infinite: true,
      speed: 500,
      fade: true,
      cssEase: 'linear'
    });
  }
}

