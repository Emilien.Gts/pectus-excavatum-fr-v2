<?php

namespace App\Controller;

use App\Model\DeleteAccount;
use App\Model\ModifyPassword;
use App\Form\Account\DeleteAccountType;
use App\Form\Account\ModifyPasswordType;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\Account\ModifyInformationsType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

#[Route('/mon-compte')]
class AccountController extends AbstractController
{
    /**
     * Access to account main page
     *
     * @return Response
     */
    #[Route('', name: 'account_index', methods: ['GET'])]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        return $this->render('pages/account/index.html.twig', [
            'user' => $this->getUser()
        ]);
    }

    /**
     * Access to the modification data's account page
     *
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    #[Route('/modifier-informations', name: 'account_modify_data', methods: ['GET', 'POST'])]
    public function modifyData(
        Request $request,
        UserPasswordEncoderInterface $encoder
    ): Response {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $form = $this->createForm(ModifyInformationsType::class, $this->getUser());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $data = $form->getData();

            if ($encoder->isPasswordValid($user, $data->getPlainPassword())) {
                $user->setFirstName($data->getFirstName());
                $user->setLastName($data->getLastName());
                $user->setPseudo($data->getPseudo());

                $this->getDoctrine()->getManager()->persist($user);
                $this->getDoctrine()->getManager()->flush();

                $this->addFlash(
                    'success',
                    'Vos informations ont bien été modifiées.'
                );
            } else {
                $this->addFlash(
                    'danger',
                    'Le mot de passe renseigné ne correspond pas à votre mot de passe actuel.'
                );
            }
        }

        return $this->render('pages/account/data.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Access to the change password's account page
     *
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    #[Route('/modifier-mot-de-passe', name: 'account_modify_password', methods: ['GET', 'POST'])]
    public function modifyPassword(
        Request $request,
        UserPasswordEncoderInterface $encoder
    ): Response {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $form = $this->createForm(ModifyPasswordType::class)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $data = $form->getData();

            if ($encoder->isPasswordValid($user, $data['actualPassword'])) {
                if ($data['plainPassword'] === $data['repeatedPassword']) {
                    $user->setPlainPassword(null);
                    $user->setPassword($encoder->encodePassword($user, $data['plainPassword']));

                    $this->getDoctrine()->getManager()->persist($user);
                    $this->getDoctrine()->getManager()->flush();

                    $this->addFlash(
                        'success',
                        'Votre mot de passe a bien été modifié.'
                    );
                } else {
                    $this->addFlash(
                        'danger',
                        'Votre nouveau mot de passe est différent de la confirmation de ce dernier.'
                    );
                }
            } else {
                $this->addFlash(
                    'danger',
                    'Le mot de passe renseigné ne correspond pas à votre mot de passe actuel.'
                );
            }
        }

        return $this->render('pages/account/password.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Access to delete account page
     *
     * @return Response
     */
    #[Route('/supprimer', name: 'account_delete', methods: ['GET', 'POST'])]
    public function delete(): Response
    {
        return $this->render('pages/account/delete.html.twig');
    }

    /**
     * Access to delete page confirmation page
     *
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    #[Route('/supprimer-confirmation', name: 'account_delete_confirmation', methods: ['GET', 'POST'])]
    public function deleteConfirmation(
        Request $request,
        UserPasswordEncoderInterface $encoder
    ): Response {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $form = $this->createForm(DeleteAccountType::class)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $data = $form->getData();

            if ($encoder->isPasswordValid($user, $data['password'])) {
                $session = $this->get('session');
                $session = new Session();
                $session->invalidate();

                $this->getDoctrine()->getManager()->remove($user);
                $this->getDoctrine()->getManager()->flush();

                $this->addFlash(
                    'success',
                    'Votre compte a bien été supprimé.'
                );

                return $this->redirectToRoute('home');
            } else {
                $this->addFlash(
                    'danger',
                    'Le mot de passe renseigné ne correspond pas à votre mot de passe actuel.'
                );
            }
        }

        return $this->render('pages/account/delete-confirmation.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
