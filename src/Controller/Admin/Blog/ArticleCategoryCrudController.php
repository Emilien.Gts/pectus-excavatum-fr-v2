<?php

namespace App\Controller\Admin\Blog;

use App\Entity\Blog\ArticleCategory;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;

class ArticleCategoryCrudController extends AbstractCrudController
{
    /**
     * Undocumented function
     *
     * @return string
     */
    public static function getEntityFqcn(): string
    {
        return ArticleCategory::class;
    }

    /**
     * Undocumented function
     *
     * @param Crud $crud
     * @return Crud
     */
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Catégorie')
            ->setEntityLabelInPlural('Catégories')
            ->setSearchFields(['name'])
            ->setDefaultSort(['id' => 'DESC', 'name' => 'DESC', 'createdAt' => 'DESC']);
    }

    /**
     * Undocumented function
     *
     * @param string $pageName
     * @return iterable
     */
    public function configureFields(string $pageName): iterable
    {
        return [
            NumberField::new('id')
                ->setFormTypeOption('disabled', 'disabled'),
            TextField::new('name')
                ->setLabel('Nom'),
            SlugField::new('slug')
                ->setLabel('Slug')
                ->setTargetFieldName('name')
                ->hideOnIndex(),
            AssociationField::new('articles')
                ->setLabel('Articles'),
            DateTimeField::new('createdAt')
                ->setLabel('Date de création')
                ->setFormTypeOption('disabled', 'disabled')
        ];
    }
}
