<?php

namespace App\Controller\Admin\Blog;

use App\Entity\Blog\ArticleComment;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ArticleCommentCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ArticleComment::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Commentaire')
            ->setEntityLabelInPlural('Commentaires')
            ->setSearchFields(['content'])
            ->setDefaultSort(['id' => 'DESC', 'content' => 'DESC', 'isApproved' => 'DESC', 'createdAt' => 'DESC']);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            NumberField::new('id')
                ->setFormTypeOption('disabled', 'disabled'),
            TextEditorField::new('content')
                ->setLabel('Commentaire'),
            TextEditorField::new('response')
                ->setLabel('Réponse')
                ->hideOnIndex(),
            BooleanField::new('isApproved')
                ->setLabel('Approuvé ?'),
            AssociationField::new('article')
                ->setLabel('Article')
                ->setFormTypeOption('disabled', 'disabled'),
            AssociationField::new('user')
                ->setLabel('Utlisateur')
                ->setFormTypeOption('disabled', 'disabled'),
            DateTimeField::new('createdAt')
                ->setLabel('Date de création')
                ->setFormTypeOption('disabled', 'disabled')
        ];
    }
}
