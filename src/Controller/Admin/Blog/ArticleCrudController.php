<?php

namespace App\Controller\Admin\Blog;

use App\Entity\Blog\Article;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ArticleCrudController extends AbstractCrudController
{
    /**
     * Undocumented function
     *
     * @return string
     */
    public static function getEntityFqcn(): string
    {
        return Article::class;
    }

    /**
     * Undocumented function
     *
     * @param Assets $assets
     * @return Assets
     */
    public function configureAssets(Assets $assets): Assets
    {
        return $assets
            ->addJsFile('build/app.js');
    }

    /**
     * Undocumented function
     *
     * @param Crud $crud
     * @return Crud
     */
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig')
            ->setEntityLabelInSingular('Article')
            ->setEntityLabelInPlural('Articles')
            ->setSearchFields(['title', 'content'])
            ->setDefaultSort(['id' => 'DESC', 'title' => 'DESC', 'isPublished' => 'DESC', 'isForward' => 'DESC', 'createdAt' => 'DESC']);
    }

    /**
     * Undocumented function
     *
     * @param string $pageName
     * @return iterable
     */
    public function configureFields(string $pageName): iterable
    {
        return [
            NumberField::new('id')
                ->setFormTypeOption('disabled', 'disabled'),
            TextField::new('title')
                ->setLabel('Titre'),
            SlugField::new('slug')
                ->setlabel('Slug')
                ->setTargetFieldName('title')
                ->hideOnIndex(),
            TextEditorField::new('content')
                ->setLabel('Contenu')
                ->setFormType(CKEditorType::class)
                ->hideOnIndex(),
            IntegerField::new('readingTime')
                ->setLabel('Temps de lecture')
                ->setFormTypeOption('disabled', 'disabled')
                ->hideOnIndex(),
            BooleanField::new('isPublished')
                ->setLabel('Publié ?'),
            BooleanField::new('isForward')
                ->setLabel('Mis en avant ?'),
            TextField::new('imageFile')
                ->setLabel('Image')
                ->setFormType(VichImageType::class)
                ->onlyOnForms(),
            ImageField::new('file')
                ->setBasePath('/uploads/articles/')
                ->hideOnForm()
                ->hideOnIndex(),
            AssociationField::new('categories')
                ->setLabel('Catégories'),
            DateTimeField::new('createdAt')
                ->setLabel('Date de création')
        ];
    }
}
