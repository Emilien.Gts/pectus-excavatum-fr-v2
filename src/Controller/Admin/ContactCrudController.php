<?php

namespace App\Controller\Admin;

use App\Entity\Contact;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ContactCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Contact::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInPlural('Contact')
            ->setSearchFields(['firstName', 'lastName', 'email', 'subject', 'message'])
            ->setDefaultSort(['id' => 'DESC', 'firstName' => 'DESC', 'lastName' => 'DESC', 'email' => 'DESC', 'subject' => 'DESC', 'sendAt' => 'DESC']);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            NumberField::new('id')
                ->setFormTypeOption('disabled', 'disabled'),
            TextField::new('firstName'),
            TextField::new('lastName'),
            TextField::new('email'),
            TextField::new('subject'),
            TextareaField::new('message')
                ->hideOnIndex(),
            DateTimeField::new('sendAt')
                ->setFormTypeOption('disabled', 'disabled')
        ];
    }
}
