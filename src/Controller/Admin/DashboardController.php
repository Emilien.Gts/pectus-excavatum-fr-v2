<?php

namespace App\Controller\Admin;

use App\Entity\Blog\Article;
use App\Entity\Blog\ArticleCategory;
use App\Entity\Blog\ArticleComment;
use App\Entity\Contact;
use App\Entity\Ebook\Ebook;
use App\Entity\Ebook\EbookChapter;
use App\Entity\Ebook\EbookOpinion;
use App\Entity\Faq\Faq;
use App\Entity\Faq\FaqCategory;
use App\Entity\Testimonial\Testimonial;
use App\Entity\Testimonial\TestimonialComment;
use App\Entity\User;
use App\Repository\Blog\ArticleCommentRepository;
use App\Repository\ContactRepository;
use App\Repository\Testimonial\TestimonialCommentRepository;
use App\Repository\UserRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    private ArticleCommentRepository $articleCommentRepository;

    private TestimonialCommentRepository $testimonialCommentRepository;

    private ContactRepository $contactRepository;

    private UserRepository $userRepository;

    public function __construct(
        ArticleCommentRepository $articleCommentRepository,
        TestimonialCommentRepository $testimonialCommentRepository,
        ContactRepository $contactRepository,
        UserRepository $userRepository
    ) {
        $this->articleCommentRepository = $articleCommentRepository;
        $this->testimonialCommentRepository = $testimonialCommentRepository;
        $this->contactRepository = $contactRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Undocumented function
     *
     * @return Response
     */
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig', [
            'articlesComments' => $this->articleCommentRepository->findUnapprovedComments(),
            'testimonialsComments' => $this->testimonialCommentRepository->findUnapprovedComments(),
            'contacts' => $this->contactRepository->findLastFiveComments(),
            'users' => $this->userRepository->findDeleteableUser()
        ]);
    }

    /**
     * Undocumented function
     *
     * @return Dashboard
     */
    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Pectus Excavatum FR');
    }

    /**
     * Undocumented function
     *
     * @return iterable
     */
    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Tableau de bord', 'fa fa-home');
        yield MenuItem::linkToCrud('Utilisateurs', 'fa fa-user', User::class);
        yield MenuItem::subMenu('Blog', 'fa fa-newspaper')->setSubItems([
            MenuItem::linkToCrud('Articles', 'fa fa-tags', Article::class),
            MenuItem::linkToCrud('Catégories', 'fa fa-th', ArticleCategory::class),
            MenuItem::linkToCrud('Commentaires', 'fa fa-comments', ArticleComment::class)
        ]);
        yield MenuItem::subMenu('Ebooks', 'fa fa-book')->setSubItems([
            MenuItem::linkToCrud('Ebooks', 'fa fa-book', Ebook::class),
            MenuItem::linkToCrud('Chapitres', 'fa fa-th', EbookChapter::class),
            MenuItem::linkToCrud('Avis', 'fa fa-comments', EbookOpinion::class)
        ]);
        yield MenuItem::subMenu('Témoignages', 'fa fa-address-card')->setSubItems([
            MenuItem::linkToCrud('Témoignages', 'fa fa-address-card', Testimonial::class),
            MenuItem::linkToCrud('Commentaires', 'fa fa-comments', TestimonialComment::class)
        ]);
        yield MenuItem::subMenu('FAQs', 'fa fa-question-circle')->setSubItems([
            MenuItem::linkToCrud('FAQs', 'fa fa-question', Faq::class),
            MenuItem::linkToCrud('Catégories', 'fa fa-th', FaqCategory::class)
        ]);
        yield MenuItem::linkToCrud('Contact', 'fa fa-address-book', Contact::class);
    }
}
