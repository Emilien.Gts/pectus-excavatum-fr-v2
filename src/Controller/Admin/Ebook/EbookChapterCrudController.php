<?php

namespace App\Controller\Admin\Ebook;

use App\Entity\Ebook\EbookChapter;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class EbookChapterCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return EbookChapter::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig')
            ->setEntityLabelInPlural('Chapitres')
            ->setEntityLabelInSingular('Chapitre d\'Ebook')
            ->setSearchFields(['title', 'content'])
            ->setDefaultSort(['id' => 'DESC', 'title' => 'DESC', 'content' => 'DESC', 'createdAt' => 'DESC']);
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            NumberField::new('id')
                ->setFormTypeOption('disabled', 'disabled'),
            TextField::new('title'),
            TextEditorField::new('content')
                ->setLabel('Contenu')
                ->setFormType(CKEditorType::class)
                ->hideOnIndex(),
            AssociationField::new('ebook'),
            DateTimeField::new('createdAt')
                ->setFormTypeOption('disabled', 'disabled')
        ];
    }
}
