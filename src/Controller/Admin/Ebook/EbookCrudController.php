<?php

namespace App\Controller\Admin\Ebook;

use App\Entity\Ebook\Ebook;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class EbookCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Ebook::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig')
            ->setEntityLabelInPlural('Ebooks')
            ->setSearchFields(['title', 'introduction', 'description'])
            ->setDefaultSort(['id' => 'DESC', 'title' => 'DESC', 'isPublished' => 'DESC', 'createdAt' => 'DESC']);
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            NumberField::new('id')
                ->setFormTypeOption('disabled', 'disabled'),
            TextField::new('title'),
            SlugField::new('slug')
                ->setTargetFieldName('title')
                ->hideOnIndex(),
            TextField::new('subtitle'),
            TextEditorField::new('introduction')
                ->setLabel('Introduction')
                ->setFormType(CKEditorType::class)
                ->hideOnIndex(),
            TextEditorField::new('description')
                ->setLabel('Description')
                ->setFormType(CKEditorType::class)
                ->hideOnIndex(),
            BooleanField::new('isPublished'),
            TextField::new('imageFile')
                ->setFormType(VichImageType::class)
                ->onlyOnForms(),
            ImageField::new('file')
                ->setBasePath('/uploads/ebooks/')
                ->hideOnForm(),
            AssociationField::new('chapters'),
            AssociationField::new('opinions'),
            DateTimeField::new('createdAt')
                ->setFormTypeOption('disabled', 'disabled')
        ];
    }
}
