<?php

namespace App\Controller\Admin\Ebook;

use App\Entity\Ebook\EbookOpinion;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class EbookOpinionCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return EbookOpinion::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig')
            ->setEntityLabelInPlural('Avis')
            ->setEntityLabelInSingular('Avis')
            ->setSearchFields(['fullName', 'city', 'opinion'])
            ->setDefaultSort(['id' => 'DESC', 'fullName' => 'DESC', 'opinion' => 'DESC', 'createdAt' => 'DESC']);
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            NumberField::new('id')
                ->setFormTypeOption('disabled', 'disabled'),
            TextField::new('fullName')->setLabel('Nom complet'),
            TextField::new('city')->setLabel('Ville'),
            TextEditorField::new('opinion')
                ->setLabel('Avis')
                ->setFormType(CKEditorType::class)
                ->hideOnIndex(),
            AssociationField::new('ebook'),
            DateTimeField::new('createdAt')
                ->setFormTypeOption('disabled', 'disabled')
        ];
    }
}
