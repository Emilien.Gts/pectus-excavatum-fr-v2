<?php

namespace App\Controller\Admin\Faq;

use App\Entity\Faq\Faq;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class FaqCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Faq::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig')
            ->setEntityLabelInPlural('FAQs')
            ->setEntityLabelInSingular('FAQ')
            ->setSearchFields(['question', 'response'])
            ->setDefaultSort(['id' => 'DESC', 'question' => 'DESC', 'response' => 'DESC', 'isPublished' => 'DESC', 'createdAt' => 'DESC']);
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            NumberField::new('id')
                ->setFormTypeOption('disabled', 'disabled'),
            TextField::new('question'),
            SlugField::new('slug')
                ->setTargetFieldName('question')
                ->hideOnIndex(),
            TextEditorField::new('response')
                ->setLabel('Réponse')
                ->setFormType(CKEditorType::class)
                ->hideOnIndex(),
            BooleanField::new('isPublished'),
            AssociationField::new('categories'),
            DateTimeField::new('publishedAt')
                ->setFormTypeOption('disabled', 'disabled'),
            DateTimeField::new('createdAt')
                ->setFormTypeOption('disabled', 'disabled')
        ];
    }
}
