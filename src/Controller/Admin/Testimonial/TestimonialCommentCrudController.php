<?php

namespace App\Controller\Admin\Testimonial;

use App\Entity\Testimonial\TestimonialComment;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class TestimonialCommentCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return TestimonialComment::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Commentaire')
            ->setEntityLabelInPlural('Commentaires')
            ->setSearchFields(['content'])
            ->setDefaultSort(['id' => 'DESC', 'content' => 'DESC', 'isApproved' => 'DESC', 'createdAt' => 'DESC']);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            NumberField::new('id')
                ->setFormTypeOption('disabled', 'disabled'),
            TextEditorField::new('content'),
            TextEditorField::new('response')
                ->hideOnIndex(),
            BooleanField::new('isApproved'),
            AssociationField::new('testimonial')
                ->setFormTypeOption('disabled', 'disabled'),
            AssociationField::new('user')
                ->setFormTypeOption('disabled', 'disabled'),
            DateTimeField::new('createdAt')
                ->setFormTypeOption('disabled', 'disabled')
        ];
    }
}
