<?php

namespace App\Controller\Admin\Testimonial;

use App\Entity\Testimonial\Testimonial;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class TestimonialCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Testimonial::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig')
            ->setEntityLabelInSingular('Témoignage')
            ->setEntityLabelInPlural('Témoignages')
            ->setSearchFields(['title', 'fullName', 'testimonial'])
            ->setDefaultSort(['id' => 'DESC', 'title' => 'DESC', 'fullName' => 'DESC', 'testimonial' => 'DESC', 'createdAt' => 'DESC']);
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            NumberField::new('id')
                ->setFormTypeOption('disabled', 'disabled'),
            TextField::new('title')
                ->setLabel('Titre'),
            SlugField::new('slug')
                ->setLabel('Slug')
                ->setTargetFieldName('title')
                ->hideOnIndex(),
            IntegerField::new('readingTime')
                ->setLabel('Temps de lecture')
                ->setFormTypeOption('disabled', 'disabled')
                ->hideOnIndex(),
            TextField::new('fullName')
                ->setLabel('Nom / Pseudo')
                ->hideOnIndex(),
            BooleanField::new('isMan')
                ->setLabel('La personne est un garçon ?')
                ->hideOnIndex(),
            TextField::new('city')
                ->setLabel('Ville')
                ->hideOnIndex(),
            TextEditorField::new('testimonial')
                ->setLabel('Contenu')
                ->setFormType(CKEditorType::class)
                ->hideOnIndex(),
            BooleanField::new('isPublished')
                ->setLabel('Est publié ?'),
            TextField::new('imageFile')
                ->setLabel('Image de couverture')
                ->setFormType(VichImageType::class)
                ->onlyOnForms(),
            ImageField::new('file')
                ->setBasePath('/uploads/articles/')
                ->hideOnForm(),
            DateTimeField::new('createdAt')
                ->setLabel('Date de création')
        ];
    }
}
