<?php

namespace App\Controller;

use App\Entity\Blog\Article;
use App\Form\Blog\CommentType;
use App\Entity\Blog\ArticleComment;
use App\Entity\Blog\ArticleCategory;
use App\Repository\Blog\ArticleRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\Blog\ArticleCommentRepository;
use App\Repository\Blog\ArticleCategoryRepository;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/blog')]
class BlogController extends AbstractController
{
    /**
     * @var ArticleRepository
     */
    private ArticleRepository $articleRepository;

    /**
     * @var Breadcrumbs
     */
    private Breadcrumbs $breadcrumbs;

    /**
     * Undocumented function
     *
     * @param ArticleRepository $articleRepository
     * @param Breadcrumbs $breadcrumbs
     */
    public function __construct(
        ArticleRepository $articleRepository,
        Breadcrumbs $breadcrumbs
    ) {
        $this->articleRepository = $articleRepository;
        $this->breadcrumbs = $breadcrumbs;
    }

    /**
     * Access to Blog page
     *
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    #[Route('', name: 'blog_index', methods: ['GET'])]
    public function index(
        Request $request,
        PaginatorInterface $paginator
    ): Response {
        $this->breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $this->breadcrumbs->addItem("Blog", $this->get("router")->generate("blog_index"));

        $articles = $paginator->paginate(
            $this->articleRepository->findSimpleArticles(),
            $request->query->getInt('page', 1),
            6
        );

        return $this->render('pages/blog/index.html.twig', [
            'featuredArticles' => $this->articleRepository->findFeaturedArticles(),
            'articles' => $articles
        ]);
    }

    /**
     * Acces to category page
     *
     * @param Request $request
     * @param ArticleCategory $category
     * @param ArticleCategoryRepository $articleCategoryRepository
     * @param PaginatorInterface $paginator
     * @return Response
     */
    #[Route('/categorie/{slug}', name: 'blog_category', methods: ['GET'])]
    public function showCategory(
        Request $request,
        ArticleCategory $category,
        ArticleCategoryRepository $articleCategoryRepository,
        PaginatorInterface $paginator,
    ): Response {
        $this->breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $this->breadcrumbs->addItem("Blog", $this->get("router")->generate("blog_index"));
        $this->breadcrumbs->addRouteItem($category->getName(), "blog_category", [
            'slug' => $category->getSlug(),
        ]);

        $articles = $paginator->paginate(
            $this->articleRepository->findArticles($category),
            $request->query->getInt('page', 1),
            9
        );

        return $this->render('pages/blog/categories.html.twig', [
            'category' => $category,
            'articles' => $articles,
            'categories' => $articleCategoryRepository->findAll()
        ]);
    }

    /**
     * Allows access to an article page
     *
     * @param Request $request
     * @param Article $article
     * @param ArticleCommentRepository $commentRepository
     * @return Response
     */
    #[Route('/{slug}', name: 'blog_show', methods: ['GET', 'POST'])]
    public function show(
        Request $request,
        Article $article,
        ArticleCommentRepository $commentRepository,
    ): Response {
        $category = $article->getCategories()->first();
        $this->breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $this->breadcrumbs->addItem("Blog", $this->get("router")->generate("blog_index"));
        $this->breadcrumbs->addRouteItem($category->getName(), 'blog_category', ['slug' => $category->getSlug()]);
        $this->breadcrumbs->addRouteItem($article->getTitle(), "blog_show", [
            'slug' => $article->getSlug(),
        ]);

        $comment = new ArticleComment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setArticle($article)
                ->setUser($this->getUser());

            $this->getDoctrine()->getManager()->persist($comment);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Merci pour votre commentaire ! Ce dernier a été soumis à l\'administration.');

            return $this->redirectToRoute('blog_show', ['slug' => $article->getSlug()]);
        }

        return $this->render('pages/blog/show.html.twig', [
            'article' => $article,
            'comments' => $commentRepository->findComments($article),
            'similarArticles' => $this->articleRepository->findSimilarArticles($article),
            'form' => $form->createView()
        ]);
    }
}
