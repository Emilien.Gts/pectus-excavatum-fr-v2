<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class ContactController extends AbstractController
{
    /**
     * Undocumented function
     *
     * @param Request $request
     * @param MailerInterface $mailer
     * @return Response
     */
    #[Route('/contact', name: 'contact', methods: ['GET', 'POST'])]
    public function index(
        Request $request,
        MailerInterface $mailer,
        Breadcrumbs $breadcrumbs
    ): Response {
        $breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $breadcrumbs->addItem("Contact", $this->get("router")->generate("contact"));

        $contact = new Contact();
        $user = $this->getUser();

        if ($user) {
            $contact->setFirstName($user->getFirstName())
                ->setLastName($user->getLastName())
                ->setEmail($user->getEmail());
        }

        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($contact);
            $this->getDoctrine()->getManager()->flush($contact);

            $email = (new TemplatedEmail())
                ->from('no-reply@pectusexcavatum.fr')
                ->to($contact->getEmail())
                ->subject('[PEFR][CONTACT] - ' . $contact->getSubject())
                ->htmlTemplate('emails/contact.html.twig')
                ->context([
                    'data' => $contact
                ]);

            $mailer->send($email);

            $this->addFlash(
                'success',
                'Votre demande de contact a bien été envoyé, elle sera traitée sous peu.'
            );
        }

        return $this->render('pages/contact.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
