<?php

namespace App\Controller;

use App\Entity\Ebook\Ebook;
use App\Form\Ebook\DownloadType;
use App\Service\MailchimpService;
use App\Repository\UserRepository;
use App\Repository\Ebook\EbookRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/ebook')]
class EbookController extends AbstractController
{
    /**
     * @var MailchimpService
     */
    private MailchimpService $mailchimpService;

    /**
     * @var Breadcrumbs
     */
    private Breadcrumbs $breadcrumbs;

    /**
     * Undocumented function
     *
     * @param Breadcrumbs $breadcrumbs
     */
    public function __construct(Breadcrumbs $breadcrumbs)
    {
        $this->mailchimpService = new MailchimpService();
        $this->breadcrumbs = $breadcrumbs;
    }

    /**
     * Access to Ebooks page
     *
     * @param Request $request
     * @param EbookRepository $ebookRepository
     * @param PaginatorInterface $paginator
     * @return Response
     */
    #[Route('', name: 'ebook_index', methods: ['GET'])]
    public function index(
        Request $request,
        EbookRepository $ebookRepository,
        PaginatorInterface $paginator
    ): Response {
        $this->breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $this->breadcrumbs->addItem("Ebooks", $this->get("router")->generate("ebook_index"));

        $ebooks = $paginator->paginate(
            $ebookRepository->findEbooks(),
            $request->query->getInt('page', 1),
            4
        );

        return $this->render('pages/ebook/index.html.twig', [
            'ebooks' => $ebooks,
        ]);
    }

    /**
     * Access to an Ebook page
     *
     * @param Request $request
     * @param Ebook $ebook
     * @param MailerInterface $mailer
     * @param UserRepository $userRepository
     * @return Response
     */
    #[Route('/{slug}', name: 'ebook_show', methods: ['GET', 'POST'])]
    public function show(
        Request $request,
        Ebook $ebook,
        MailerInterface $mailer,
        UserRepository $userRepository
    ): Response {
        $this->breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $this->breadcrumbs->addItem("Ebooks", $this->get("router")->generate("ebook_index"));
        $this->breadcrumbs->addRouteItem($ebook->getTitle(), "ebook_show", [
            'slug' => $ebook->getSlug(),
        ]);

        $form = $this->createForm(DownloadType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->getUser()) return $this->redirect('/uploads/ebook/' . $ebook->getSlug());

            try {
                $data = $form->getData();
                $email = (new TemplatedEmail())
                    ->from('no-reply@pectusexcavatum.fr')
                    ->to($data['email'])
                    ->subject('Ebook - ' . $ebook->getTitle())
                    ->htmlTemplate('emails/ebook.html.twig')
                    ->context([
                        'ebook' => $ebook,
                        'pseudo' => $data['pseudo'],
                    ]);

                $mailer->send($email);

                (!$userRepository->findOneBy(['email' => $data['email']]) ? $this->mailchimpService->setNewMember($data) : '');

                $this->addFlash(
                    'success',
                    'Un email va vous parvenir afin que vous puissiez télécharger votre Ebook. S\'il n\'arrive pas, merci de bien vouloir contacter l\'administrateur.'
                );

                return $this->redirectToRoute('ebook_index');
            } catch (\Throwable $th) {
                $this->addFlash(
                    'danger',
                    'Une erreur s\'est produite. Il se peut que vous aillez reçu votre email, sinon veuillez contacter l\'administrateur.'
                );

                return $this->redirectToRoute('ebook_show', [
                    'slug' => $ebook->getSlug()
                ]);
            }
        }

        return $this->render('pages/ebook/show.html.twig', [
            'ebook' => $ebook,
            'chapters' => $ebook->getChapters(),
            'opinions' => $ebook->getOpinions(),
            'form' => $form->createView()
        ]);
    }
}
