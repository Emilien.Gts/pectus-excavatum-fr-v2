<?php

namespace App\Controller;

use App\Entity\Faq\Faq;
use App\Entity\Faq\FaqCategory;
use App\Form\Faq\SearchType;
use App\Repository\Faq\FaqRepository;
use Knp\Component\Pager\PaginatorInterface;
use App\Repository\Faq\FaqCategoryRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

#[Route('/faq')]
class FaqController extends AbstractController
{
    /**
     * @var FaqCategoryRepository
     */
    private FaqCategoryRepository $categoryRepositiory;

    /**
     * @var Breadcrumbs
     */
    private Breadcrumbs $breadcrumbs;

    /**
     * Undocumented function
     *
     * @param FaqCategoryRepository $categoryRepositiory
     * @param Breadcrumbs $breadcrumbs
     */
    public function __construct(
        FaqCategoryRepository $categoryRepositiory,
        Breadcrumbs $breadcrumbs
    ) {
        $this->categoryRepositiory = $categoryRepositiory;
        $this->breadcrumbs = $breadcrumbs;
    }

    /**
     * Allow access to faq page
     *
     * @param Request $request
     * @param FaqRepository $faqRepository
     * @param PaginatorInterface $paginator
     * @return Response
     */
    #[Route('', name: 'faq_index', methods: ['GET', 'POST'])]
    public function index(
        Request $request,
        FaqRepository $faqRepository,
        PaginatorInterface $paginator
    ): Response {
        $this->breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $this->breadcrumbs->addItem("Ressources", $this->get("router")->generate("ressources"));
        $this->breadcrumbs->addItem("FAQ", $this->get("router")->generate("faq_index"));

        $form = $this->createForm(SearchType::class)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $searchRequest = $form->get("search")->getData();

            $faqs = $paginator->paginate(
                $faqRepository->getSearch($searchRequest),
                $request->query->getInt('page', 1),
                5
            );

            return $this->render('pages/faq/search.html.twig', [
                'faqs' => $faqs,
                'searchRequest' => $searchRequest
            ]);
        }

        return $this->render('pages/faq/index.html.twig', [
            'categories' => $this->categoryRepositiory->findAll(),
            'form' => $form->createView()
        ]);
    }

    /**
     * Allow access to a category's FAQ page
     *
     * @param Request $request
     * @param FaqCategory $category
     * @param PaginatorInterface $paginator
     * @param FaqRepository $faqRepository
     * @return Response
     */
    #[Route('/categorie/{slug}', name: 'faq_show_category', methods: ['GET'])]
    public function showCategory(
        Request $request,
        FaqCategory $category,
        PaginatorInterface $paginator,
        FaqRepository $faqRepository
    ): Response {
        $this->breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $this->breadcrumbs->addItem("Ressources", $this->get("router")->generate("ressources"));
        $this->breadcrumbs->addItem("FAQ", $this->get("router")->generate("faq_index"));
        $this->breadcrumbs->addRouteItem($category->getName(), 'faq_show_category', ['slug' => $category->getSlug()]);

        $faqs = $paginator->paginate(
            $faqRepository->getPublishedQuestions($category),
            $request->query->getInt('page', 1),
            5
        );

        return $this->render('pages/faq/category.html.twig', [
            'category' => $category,
            'faqs' => $faqs,
        ]);
    }

    /**
     * Allow access to a specific FAQ
     *
     * @param Faq $faq
     * @return Response
     */
    #[Route('/{slug}', name: 'faq_show_faq', methods: ['GET'])]
    public function showQuestion(Faq $faq): Response
    {
        $category = $faq->getCategories()->first();
        $this->breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $this->breadcrumbs->addItem("Ressources", $this->get("router")->generate("ressources"));
        $this->breadcrumbs->addItem("FAQ", $this->get("router")->generate("faq_index"));
        $this->breadcrumbs->addRouteItem($category->getName(), 'faq_show_category', ['slug' => $category->getSlug()]);
        $this->breadcrumbs->addRouteItem($faq->getQuestion(), 'faq_show_faq', ['slug' => $faq->getSlug()]);

        return $this->render('pages/faq/faq.html.twig', [
            'faq' => $faq
        ]);
    }
}
