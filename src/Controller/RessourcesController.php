<?php

namespace App\Controller;

use App\Repository\Blog\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class RessourcesController extends AbstractController
{
    /**
     * @var Breadcrumbs
     */
    private Breadcrumbs $breadcrumbs;

    /**
     * Undocumented function
     *
     * @param Breadcrumbs $breadcrumbs
     */
    public function __construct(Breadcrumbs $breadcrumbs)
    {
        $this->breadcrumbs = $breadcrumbs;
    }

    /**
     * Allow access to ressources pages
     *
     * @param ArticleRepository $articleRepository
     * @return Response
     */
    #[Route('/ressources', name: 'ressources')]
    public function index(): Response
    {
        $this->breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $this->breadcrumbs->addItem("Ressources", $this->get("router")->generate("ressources"));

        return $this->render('pages/ressources/index.html.twig');
    }

    /**
     * Allow access to graphic chart page
     *
     * @return Response
     */
    #[Route('/ressources/charte-graphique', name: 'graphic_charter')]
    public function graphicCharter(): Response
    {
        $this->breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $this->breadcrumbs->addItem("Ressources", $this->get("router")->generate("ressources"));
        $this->breadcrumbs->addItem("Charte graphique", $this->get("router")->generate("graphic_charter"));

        return $this->render('pages/ressources/graphic_charter.html.twig');
    }

    /**
     * Allow access to about page
     *
     * @param Breadcrumbs $breadcrumbs
     * @return Response
     */
    #[Route('/a-propos', name: 'about')]
    public function about(Breadcrumbs $breadcrumbs): Response
    {
        $breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $breadcrumbs->addItem("Ressources", $this->get("router")->generate("ressources"));
        $breadcrumbs->addItem("A propos", $this->get("router")->generate("about"));

        return $this->render('pages/ressources/about.html.twig');
    }
}
