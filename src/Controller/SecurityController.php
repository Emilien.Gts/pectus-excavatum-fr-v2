<?php

namespace App\Controller;

use App\Entity\User;
use Ramsey\Uuid\Uuid;
use App\Service\MailchimpService;
use App\Repository\UserRepository;
use App\Form\Security\ResetPasswordType;
use App\Form\Security\ForgottenPasswordType;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use App\Form\Authentication\RegistrationType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
    /**
     * Undocumented function
     *
     * @param Request $request
     * @param MailerInterface $mailer
     * @param UserRepository $userRepository
     * @return Response
     */
    #[Route('/mot-de-passe-oublie', name: 'security_forgotten_password', methods: ['GET', 'POST'])]
    public function forgottenPassword(
        Request $request,
        MailerInterface $mailer,
        UserRepository $userRepository
    ): Response {
        $form = $this->createForm(ForgottenPasswordType::class)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $userRepository->findOneBy(["email" => $form->get("email")->getData()]);

            if ($user !== null) {
                $token = Uuid::uuid4()->toString();
                $user->setForgottenPasswordToken($token);

                $this->getDoctrine()->getManager()->persist($user);
                $this->getDoctrine()->getManager()->flush();

                $email = (new TemplatedEmail())
                    ->from('no-reply@pectusexcavatum.fr')
                    ->to($user->getEmail())
                    ->subject('Réinitialisation de votre mot de passe')
                    ->htmlTemplate('emails/forgotten_password.html.twig')
                    ->context([
                        'token' => $token,
                        'user' => $user
                    ]);

                $mailer->send($email);

                $this->addFlash(
                    "success",
                    sprintf(
                        "Un email a été envoyé à %s avec la procédure à suivre pour réinitialiser votre mot de passe",
                        $user->getEmail()
                    )
                );
            } else {
                $this->addFlash(
                    'danger',
                    'Cette adresse email n\'est relié à aucun compte connu sur ce site web.'
                );

                return $this->render('pages/security/forgotten_password.html.twig', [
                    'form' => $form->createView()
                ]);
            }

            return $this->redirectToRoute("security_forgotten_password");
        }

        return $this->render('pages/security/forgotten_password.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Undocumented function
     *
     * @param User $user
     * @param Request $request
     * @param UserPasswordEncoderInterface $userPasswordEncoder
     * @return Response
     */
    #[Route('/reinitialiser-mot-de-passe/{forgottenPasswordToken}', name: 'security_reset_password', methods: ['GET', 'POST'])]
    public function resetPassword(
        User $user,
        Request $request,
        UserPasswordEncoderInterface $userPasswordEncoder
    ): Response {
        $form = $this->createForm(ResetPasswordType::class)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($userPasswordEncoder->encodePassword($user, $form->get("plainPassword")->getData()));
            $user->setForgottenPasswordToken(null);

            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash("success", "Votre mot de passe a été modifié avec succès.");

            return $this->redirectToRoute("app_login");
        }

        return $this->render('pages/security/reset_password.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Login an user
     *
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    #[Route('/connexion', name: 'app_login')]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('home');
        }

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('pages/security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

    /**
     * Logout an user
     *
     * @return void
     */
    #[Route('/deconnexion', name: 'app_logout')]
    public function logout(): void
    {
        // Nothing to do here..
    }

    /**
     * Enables the registration of an user
     *
     * @param Request $request
     * @param MailchimpService $mailchimpService
     * @return Response
     */
    #[Route('/inscription', name: 'app_register')]
    public function register(Request $request, MailchimpService $mailchimpService): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $form->getData()->getPseudo() === '' ? $user->setPseudo($user->getFirstName()) : '';

            $mailchimpService->setNewMember([
                'email' => $user->getEmail(),
                'pseudo' => $user->getPseudo(),
            ]);

            $password = $form->getData()->getPlainPassword();
            $user->setPlainPassword($password);

            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('app_login');
        }

        return $this->render('pages/security/registration.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Access to privacy policy page
     *
     * @return Response
     */
    #[Route('/politique-confidentialite', name: 'app_privacy_policy')]
    public function privacyPolicy(): Response
    {
        return $this->render('pages/security/privacy.html.twig');
    }

    /**
     * Access to legal mentions page
     *
     * @return Response
     */
    #[Route('/mentions-legales', name: 'app_legal_notices')]
    public function legalNotices(): Response
    {
        return $this->render('pages/security/legal-notices.html.twig');
    }

    /**
     * Access to terms service
     *
     * @return Response
     */
    #[Route('/conditions-generales-utilisation', name: 'app_terms_service')]
    public function termsService(): Response
    {
        return $this->render('pages/security/terms-service.html.twig');
    }
}
