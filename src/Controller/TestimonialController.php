<?php

namespace App\Controller;

use App\Form\Testimonial\CommentType;
use App\Entity\Testimonial\Testimonial;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Testimonial\TestimonialComment;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\Testimonial\TestimonialRepository;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;
use App\Repository\Testimonial\TestimonialCommentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/temoignage')]
class TestimonialController extends AbstractController
{
    /**
     * @var TestimonialRepository
     */
    private TestimonialRepository $testimonialRepository;

    /**
     * @var Breadcrumbs
     */
    private Breadcrumbs $breadcrumbs;

    /**
     * Undocumented function
     *
     * @param TestimonialRepository $testimonialRepository
     */
    public function __construct(
        TestimonialRepository $testimonialRepository,
        Breadcrumbs $breadcrumbs
    ) {
        $this->testimonialRepository = $testimonialRepository;
        $this->breadcrumbs = $breadcrumbs;
    }

    /**
     * Allow access to the testimonials page
     *
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    #[Route('', name: 'testimonial_index', methods: ['GET'])]
    public function index(
        Request $request,
        PaginatorInterface $paginator
    ): Response {
        $this->breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $this->breadcrumbs->addItem("Témoignages", $this->get("router")->generate("testimonial_index"));

        $testimonials = $paginator->paginate(
            $this->testimonialRepository->findSimpleTestimonials(),
            $request->query->getInt('page', 1),
            9
        );

        return $this->render('pages/testimonial/index.html.twig', [
            'testimonials' => $testimonials,
        ]);
    }

    /**
     * Allow access to a testimonial page
     *
     * @param Request $request
     * @param Testimonial $testimonial
     * @param TestimonialCommentRepository $commentRepository
     * @return Response
     */
    #[Route('/{slug}', name: 'testimonial_show', methods: ['GET', 'POST'])]
    public function show(
        Request $request,
        Testimonial $testimonial,
        TestimonialCommentRepository $commentRepository
    ): Response {
        $this->breadcrumbs->addItem("Accueil", $this->get("router")->generate("home"));
        $this->breadcrumbs->addItem("Témoignages", $this->get("router")->generate("testimonial_index"));
        $this->breadcrumbs->addRouteItem($testimonial->getTitle(), "testimonial_show", [
            'slug' => $testimonial->getSlug(),
        ]);

        $similarTestimonials = $this->testimonialRepository->findLastThreeTestimonials($testimonial);

        $comment = new TestimonialComment();
        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setTestimonial($testimonial)
                ->setUser($this->getUser());

            $this->getDoctrine()->getManager()->persist($comment);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'success',
                'Merci pour votre commentaire ! Ce dernier a été soumis à l\'administration.'
            );

            return $this->redirectToRoute('testimonial_show', [
                'slug' => $testimonial->getSlug(),
                'testimonial' => $testimonial,
                'comments' => $commentRepository->findPublishedCommentsFromATestimonial($testimonial),
                'similarTestimonials' => $similarTestimonials,
                'form' => $form->createView()
            ]);
        }

        return $this->render('pages/testimonial/show.html.twig', [
            'testimonial' => $testimonial,
            'lastTestimonials' => $similarTestimonials,
            'comments' => $commentRepository->findPublishedCommentsFromATestimonial($testimonial),
            'form' => $form->createView()
        ]);
    }
}
