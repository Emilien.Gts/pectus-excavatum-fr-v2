<?php

namespace App\Controller;

use App\Entity\Blog\Article;
use App\Entity\Blog\ArticleCategory;
use App\Entity\Ebook\Ebook;
use App\Entity\Faq\Faq;
use App\Entity\Faq\FaqCategory;
use App\Entity\Testimonial\Testimonial;
use App\Repository\Ebook\EbookRepository;
use App\Repository\Blog\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\Testimonial\TestimonialRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class WebsiteController extends AbstractController
{
    /**
     * Allow access to the homepage
     *
     * @param ArticleRepository $articleRepository
     * @return Response
     */
    #[Route('/', name: 'home')]
    public function index(
        ArticleRepository $articleRepository,
        EbookRepository $ebookRepository,
        TestimonialRepository $testimonialRepository
    ): Response {
        return $this->render('pages/home/index.html.twig', [
            'nbArticles' => count($articleRepository->findAll()),
            'nbEbooks' => count($ebookRepository->findAll()),
            'nbTestimonials' => count($testimonialRepository->findAll()),
            'articles' => $articleRepository->findLastArticles(2),
            'testimonials' => $testimonialRepository->findLastTestimonials()
        ]);
    }

    /**
     * Access to robots.txt
     *
     * @return void
     */
    #[Route('/robots.txt', name: 'robots', format: 'txt')]
    public function robot()
    {
        $response = new Response(
            $this->renderView('pages/robots.txt'),
            200
        );

        $response->headers->set('Content-Type', 'text/txt');

        return $response;
    }

    /**
     * Access to sitemap.xml
     *
     * @param Request $request
     * @return Response
     */
    #[Route('/sitemap.xml', name: 'sitemap', format: 'xml')]
    public function sitemap(Request $request): Response
    {
        $urls = [];
        $hostname = $request->getSchemeAndHttpHost();

        $urls[] = ['loc' => $this->get('router')->generate('home'), 'changefreq' => 'monthly', 'priority' => '1.0'];
        $urls[] = ['loc' => $this->get('router')->generate('app_register'), 'changefreq' => 'never', 'priority' => '1.0'];
        $urls[] = ['loc' => $this->get('router')->generate('app_login'), 'changefreq' => 'never', 'priority' => '1.0'];
        $urls[] = ['loc' => $this->get('router')->generate('contact'), 'changefreq' => 'never', 'priority' => '0.8'];
        $urls[] = ['loc' => $this->get('router')->generate('about'), 'changefreq' => 'never', 'priority' => '1.0'];

        // Articles
        $urls[] = ['loc' => $this->get('router')->generate('blog_index'), 'changefreq' => 'monthly', 'priority' => '1.0'];

        foreach ($this->getDoctrine()->getRepository(ArticleCategory::class)->findAll() as $category) {
            $urls[] = [
                'loc' => $this->generateUrl('blog_category', [
                    'slug' => $category->getSlug(),
                ]),
                'priority' => '0.2',
                'changefreq' => 'yearly',
                'lastmod' => $category->getCreatedAt()->format('Y-m-d'),
            ];
        }

        foreach ($this->getDoctrine()->getRepository(Article::class)->findAll() as $article) {
            $images = [
                'loc' => '/uploads/articles/' . $article->getImageName(),
                'title' => $article->getImageName()
            ];

            $urls[] = [
                'loc' => $this->generateUrl('blog_show', [
                    'slug' => $article->getSlug(),
                ]),
                'priority' => '0.8',
                'changefreq' => 'yearly',
                'lastmod' => $article->getCreatedAt()->format('Y-m-d'),
                'image' => $images
            ];
        }

        // Ebooks
        $urls[] = ['loc' => $this->get('router')->generate('ebook_index'), 'changefreq' => 'monthly', 'priority' => '1.0'];
        foreach ($this->getDoctrine()->getRepository(Ebook::class)->findAll() as $ebook) {
            $images = [
                'loc' => '/uploads/ebooks/' . $ebook->getImageName(),
                'title' => $ebook->getImageName()
            ];

            $urls[] = [
                'loc' => $this->generateUrl('ebook_show', [
                    'slug' => $ebook->getSlug(),
                ]),
                'priority' => '1.0',
                'changefreq' => 'yearly',
                'lastmod' => $ebook->getCreatedAt()->format('Y-m-d'),
                'image' => $images
            ];
        }

        // FAQ
        $urls[] = ['loc' => $this->get('router')->generate('faq_index'), 'changefreq' => 'weekly', 'priority' => '0.5'];

        foreach ($this->getDoctrine()->getRepository(FaqCategory::class)->findAll() as $category) {
            $images = [
                'loc' => '/uploads/faqs/' . $category->getImageName(),
                'title' => $category->getImageName()
            ];

            $urls[] = [
                'loc' => $this->generateUrl('faq_show_category', [
                    'slug' => $category->getSlug(),
                ]),
                'priority' => '0.2',
                'changefreq' => 'never',
                'lastmod' => $category->getCreatedAt()->format('Y-m-d'),
            ];
        }

        foreach ($this->getDoctrine()->getRepository(Faq::class)->findAll() as $faq) {
            $urls[] = [
                'loc' => $this->generateUrl('faq_show_faq', [
                    'slug' => $faq->getSlug(),
                ]),
                'priority' => '0.5',
                'changefreq' => 'monthly',
                'lastmod' => $faq->getCreatedAt()->format('Y-m-d'),
            ];
        }

        // Testimonials
        $urls[] = ['loc' => $this->get('router')->generate('testimonial_index'), 'changefreq' => 'monthly', 'priority' => '1.0'];
        foreach ($this->getDoctrine()->getRepository(Testimonial::class)->findAll() as $testimonial) {
            $images = [
                'loc' => '/uploads/ebooks/' . $testimonial->getImageName(),
                'title' => $testimonial->getImageName()
            ];

            $urls[] = [
                'loc' => $this->generateUrl('testimonial_show', [
                    'slug' => $testimonial->getSlug(),
                ]),
                'priority' => '0.5',
                'changefreq' => 'yearly',
                'lastmod' => $testimonial->getCreatedAt()->format('Y-m-d'),
                'image' => $images
            ];
        }

        $urls[] = ['loc' => $this->get('router')->generate('account_modify_data'), 'changefreq' => 'never', 'priority' => '0.2'];
        $urls[] = ['loc' => $this->get('router')->generate('account_modify_password'), 'changefreq' => 'never', 'priority' => '0.2'];
        $urls[] = ['loc' => $this->get('router')->generate('account_delete'), 'changefreq' => 'never', 'priority' => '0.2'];
        $urls[] = ['loc' => $this->get('router')->generate('account_delete_confirmation'), 'changefreq' => 'never', 'priority' => '0.2'];
        $urls[] = ['loc' => $this->get('router')->generate('security_forgotten_password'), 'changefreq' => 'never', 'priority' => '0.2'];
        $urls[] = ['loc' => $this->get('router')->generate('app_privacy_policy'), 'changefreq' => 'never', 'priority' => '0.2'];
        $urls[] = ['loc' => $this->get('router')->generate('app_legal_notices'), 'changefreq' => 'never', 'priority' => '0.2'];
        $urls[] = ['loc' => $this->get('router')->generate('ressources'), 'changefreq' => 'never', 'priority' => '0.5'];
        $urls[] = ['loc' => $this->get('router')->generate('graphic_charter'), 'changefreq' => 'never', 'priority' => '0.1'];

        // Once our array is filled, we define the controller response
        $response = new Response(
            $this->renderView('pages/sitemap.html.twig', [
                'urls' => $urls,
                'hostname' => $hostname
            ]),
            200
        );

        $response->headers->set('Content-Type', 'text/xml');

        return $response;
    }
}
