<?php

namespace App\DataFixtures;

use Faker\Factory;
use Faker\Generator;
use App\Entity\Blog\Article;
use App\DataFixtures\UserFixtures;
use App\Repository\UserRepository;
use App\Entity\Blog\ArticleComment;
use App\Entity\Blog\ArticleCategory;
use Doctrine\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class BlogFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;

    /**
     * @var Generator
     */
    private Generator $faker;

    /**
     * Undocumented function
     *
     * @param EntityManagerInterface $manager
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->faker = Factory::create('fr_FR');
    }

    /**
     * Undocumented function
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [UserFixtures::class];
    }

    /**
     * Adds fixtures for Blog
     *
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        // Set articles
        $articles = [];
        for ($i = 0; $i < 50; $i++) {
            $article = new Article();
            $article->setTitle($this->faker->sentence())
                ->setReadingTime($this->faker->randomDigit(0))
                ->setContent($this->faker->text())
                ->setIsPublished($i > 1 ? (mt_rand(0, 1) === 1 ? true : false) : true)
                ->setIsForward($i > 1 ? false : true)
                ->setImageName('placeholder.png');

            $manager->persist($article);
            $articles[] = $article;
        }

        // Set categories
        $categories = [];
        for ($i = 0; $i  < 5; $i++) {
            $category = new ArticleCategory();
            $category->setName($this->faker->sentence(2));

            $manager->persist($category);
            $categories[] = $category;
        }

        // Link articles and categories
        foreach ($articles as $article) {
            $article->addCategory($categories[mt_rand(0, count($categories) - 1)]);
        }

        // Set comments
        $comments = [];
        $users = $this->userRepository->findAll();
        foreach ($articles as $article) {
            for ($i = 0; $i < 2; $i++) {
                $user = $users[mt_rand(0, count($users) - 1)];

                $comment = new ArticleComment();
                $comment->setContent($this->faker->text())
                    ->setIsApproved(mt_rand(0, 1) === 1 ? true : false)
                    ->setResponse(mt_rand(0, 1) === 1 ? $this->faker->text() : null)
                    ->setCreatedAt(new \DateTimeImmutable())
                    ->setArticle($article)
                    ->setUser($user);

                $manager->persist($comment);
                $comments[] = $comment;
            }
        }

        $manager->flush();
    }
}
