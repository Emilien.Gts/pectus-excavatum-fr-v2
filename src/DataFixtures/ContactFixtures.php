<?php

namespace App\DataFixtures;

use App\Entity\Contact;
use Faker\Factory;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Faker\Generator;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ContactFixtures extends Fixture
{
    private EntityManagerInterface $manager;

    private Generator $faker;

    /**
     * Constructor
     *
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
        $this->faker = Factory::create('fr_FR');
    }

    /**
     * Adds fixtures about the User entity to the database.
     *
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 10; $i++) {
            $contact = ((new Contact)
                ->setFirstName($this->faker->firstName())
                ->setLastName($this->faker->lastName())
                ->setEmail($this->faker->email())
                ->setAgreeTerms(true)
                ->setSubject($this->faker->word())
                ->setMessage($this->faker->text()));

            $this->manager->persist($contact);
        }

        $this->manager->flush();
    }
}
