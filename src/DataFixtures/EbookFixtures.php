<?php

namespace App\DataFixtures;

use Faker\Factory;
use Faker\Generator;
use App\Entity\Ebook\Ebook;
use App\Entity\Ebook\EbookChapter;
use App\Entity\Ebook\EbookOpinion;
use Doctrine\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;

class EbookFixtures extends Fixture
{
    /**
     * @var Generator
     */
    private Generator $faker;

    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->faker = Factory::create('fr_FR');
    }

    /**
     * Adds fixtures about the Ebook entity to the database.
     *
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        $ebooks = [];
        for ($i = 0; $i < 20; $i++) {
            $ebook = new Ebook();
            $ebook->setTitle($this->faker->sentence())
                ->setSubtitle($this->faker->sentence(3))
                ->setIntroduction($this->faker->text(500))
                ->setDescription($this->faker->text(500))
                ->setIsPublished(mt_rand(0, 2) !== 1 ? true : false)
                ->setImageName('placeholder.png');

            $manager->persist($ebook);
            $ebooks[] = $ebook;
        }

        foreach ($ebooks as $ebook) {
            for ($i = 0; $i < mt_rand(4, 7); $i++) {
                $chapter = new EbookChapter();
                $chapter->setTitle($this->faker->sentence())
                    ->setContent($this->faker->text(100))
                    ->setEbook($ebook);

                $opinion = new EbookOpinion();
                $opinion->setFullName($this->faker->name())
                    ->setCity($this->faker->city())
                    ->setOpinion($this->faker->text())
                    ->setEbook($ebook);

                $manager->persist($chapter);
                $manager->persist($opinion);
            }
        }

        $manager->flush();
    }
}
