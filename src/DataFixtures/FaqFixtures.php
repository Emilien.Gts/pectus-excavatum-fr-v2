<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Repository\UserRepository;
use App\Entity\Faq\Faq;
use App\Entity\Faq\FaqCategory;
use Doctrine\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Faker\Generator;

class FaqFixtures extends Fixture implements DependentFixtureInterface
{
    private EntityManagerInterface $manager;

    private Generator $faker;

    /**
     * Undocumented function
     *
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
        $this->faker = Factory::create('fr_FR');
    }

    /**
     * Undocumented function
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [UserFixtures::class];
    }

    /**
     * Adds fixtures about the Blog entities to the database.
     *
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        $categories = $this->setCategories();
        $this->setFaqs($categories);

        $this->manager->flush();
    }

    /**
     * Set FAQ categories
     *
     * @return void
     */
    private function setCategories(): array
    {
        $categories = [];
        for ($i = 0; $i < 10; $i++) {
            $category = new FaqCategory();
            $category->setName($this->faker->name)
                ->setImageName('placeholder.png');

            $this->manager->persist($category);
            $categories[] = $category;
        }

        return $categories;
    }

    /**
     * Set FAQs
     *
     * @param [type] $categories
     * @return void
     */
    private function setFaqs($categories): void
    {
        for ($i = 0; $i < 10; $i++) {
            $faq = new Faq();
            $faq->setQuestion($this->faker->name)
                ->setResponse($this->faker->text)
                ->setIsPublished(mt_rand(0, 1) === 1 ? true : false)
                ->addCategory($categories[mt_rand(0, count((array) $categories) - 1)]);

            $this->manager->persist($faq);
        }
    }
}
