<?php

namespace App\DataFixtures;

use Faker\Generator;
use Faker\Factory;
use App\Entity\Image;
use App\DataFixtures\UserFixtures;
use App\Repository\UserRepository;
use App\Entity\Testimonial\Testimonial;
use Doctrine\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Entity\Testimonial\TestimonialComment;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class TestimonialFixtures extends Fixture implements DependentFixtureInterface
{
    private EntityManagerInterface $manager;

    private UserRepository $userRepository;

    private Generator $faker;

    /**
     * Undocumented function
     *
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager, UserRepository $userRepository)
    {
        $this->manager = $manager;
        $this->userRepository = $userRepository;
        $this->faker = Factory::create('fr_FR');
    }

    /**
     * Undocumented function
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [UserFixtures::class];
    }

    /**
     * Adds fixtures about the Testimonial entity to the database.
     *
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        // Set testimonials
        $testimonials = [];
        for ($i = 0; $i < 50; $i++) {
            $testimonial = new Testimonial();
            $testimonial->setTitle($this->faker->sentence())
                ->setReadingTime(mt_rand(1, 6))
                ->setFullName($this->faker->name())
                ->setIsMan(mt_rand(0, 1) == 1 ? true : false)
                ->setCity($this->faker->city())
                ->setTestimonial($this->faker->sentence())
                ->setIsPublished(mt_rand(0, 1) == 1 ? true : false)
                ->setCreatedAt(new \DateTime())
                ->setImageName('placeholder.png');

            $this->manager->persist($testimonial);
            $testimonials[] = $testimonial;
        }

        // Set comments
        $comments = [];
        $users = $this->userRepository->findAll();
        foreach ($testimonials as $testimonial) {
            for ($i = 0; $i < 2; $i++) {
                $user = $users[mt_rand(0, count($users) - 1)];
                $comment = (new TestimonialComment())
                    ->setContent($this->faker->text())
                    ->setIsApproved(mt_rand(0, 1) === 1 ? true : false)
                    ->setResponse(mt_rand(0, 1) === 1 ? $this->faker->text() : null)
                    ->setCreatedAt(new \DateTime())
                    ->setTestimonial($testimonial)
                    ->setUser($user);

                $this->manager->persist($comment);
                $comments[] = $comment;
            }
        }

        $this->manager->flush();
    }
}
