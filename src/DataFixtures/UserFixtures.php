<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Faker\Generator;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private EntityManagerInterface $manager;

    private Generator $faker;

    /**
     * Constructor
     *
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
        $this->faker = Factory::create('fr_FR');
    }

    /**
     * Adds fixtures about the User entity to the database.
     *
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        $this->setEmilien();
        $this->setUsers();
        $this->manager->flush();
    }

    /**
     * Set user Emilien
     *
     * @return void
     */
    private function setEmilien(): void
    {
        $user = new User();
        $user->setFirstName('Emilien')
            ->setLastName('Gantois')
            ->setPseudo('EmilienGts')
            ->setEmail('emilien@pectusexcavatum.fr')
            ->setAgreeTerms(true)
            ->setRoles(['ROLE_USER', 'ROLE_ADMIN'])
            ->setPlainPassword('password');

        $this->manager->persist($user);
    }

    /**
     * Set users fixtures
     *
     * @return void
     */
    private function setUsers(): void
    {
        for ($i = 0; $i < 20; $i++) {
            $user = new User();
            $user->setFirstName($this->faker->firstName)
                ->setLastName($this->faker->lastName)
                ->setPseudo($this->faker->name)
                ->setEmail($this->faker->safeEmail)
                ->setAgreeTerms(true)
                ->setPlainPassword('password');

            $this->manager->persist($user);
        }
    }
}
