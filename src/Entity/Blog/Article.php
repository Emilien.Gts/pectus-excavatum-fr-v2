<?php

namespace App\Entity\Blog;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Blog\ArticleComment;
use App\Entity\Blog\ArticleCategory;
use App\Repository\Blog\ArticleRepository;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\EntityListeners({"App\EntityListener\Blog\ArticleListener"})
 * @ORM\Entity(repositoryClass=ArticleRepository::class)
 * @Vich\Uploadable
 */
#[UniqueEntity('slug')]
class Article
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 255)]
    private string $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotNull()]
    #[Assert\Length(max: 255)]
    private ?string $slug = 'le-slug';

    /**
     * @ORM\Column(type="integer")
     */
    #[Assert\NotNull()]
    #[Assert\Positive()]
    private int $readingTime = 1;

    /**
     * @ORM\Column(type="text")
     */
    #[Assert\NotBlank()]
    private string $content;

    /**
     * @ORM\Column(type="boolean")
     */
    #[Assert\NotNull()]
    private bool $isPublished = false;

    /**
     * @ORM\Column(type="boolean")
     */
    #[Assert\NotNull()]
    private bool $isForward = false;

    /**
     * @Vich\UploadableField(mapping="article_images", fileNameProperty="imageName")
     */
    private ?File $imageFile = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\Length(max: 255)]
    private $imageName;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeImmutable $createdAt;

    /**
     * @ORM\ManyToMany(targetEntity=ArticleCategory::class, inversedBy="articles")
     */
    private Collection $categories;

    /**
     * @ORM\OneToMany(targetEntity=ArticleComment::class, mappedBy="article", orphanRemoval=true)
     */
    private Collection $comments;

    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
        $this->comments = new ArrayCollection();
    }

    /**
     * Undocumented function
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Undocumented function
     *
     * @param string $title
     * @return self
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * Undocumented function
     *
     * @param string $slug
     * @return self
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return integer|null
     */
    public function getReadingTime(): ?int
    {
        return $this->readingTime;
    }

    /**
     * Undocumented function
     *
     * @param integer $readingTime
     * @return self
     */
    public function setReadingTime(int $readingTime): self
    {
        $this->readingTime = $readingTime;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * Undocumented function
     *
     * @param string $content
     * @return self
     */
    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return boolean|null
     */
    public function getIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    /**
     * Undocumented function
     *
     * @param boolean $isPublished
     * @return self
     */
    public function setIsPublished(bool $isPublished): self
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return boolean|null
     */
    public function getIsForward(): ?bool
    {
        return $this->isForward;
    }

    /**
     * Undocumented function
     *
     * @param boolean $isForward
     * @return self
     */
    public function setIsForward(bool $isForward): self
    {
        $this->isForward = $isForward;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param File|null $imageFile
     * @return void
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if ($this->imageFile !== null) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            // $this->createdAt = new \DateTimeImmutable();
        }
    }

    /**
     * Undocumented function
     *
     * @return File|null
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * Undocumented function
     *
     * @param string|null $imageName
     * @return void
     */
    public function setImageName(?string $imageName): void
    {
        $this->imageName = $imageName;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    /**
     * Undocumented function
     *
     * @return \DateTimeImmutable|null
     */
    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * Undocumented function
     *
     * @param \DateTimeImmutable $createdAt
     * @return self
     */
    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return Collection
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    /**
     * Undocumented function
     *
     * @param ArticleCategory $category
     * @return self
     */
    public function addCategory(ArticleCategory $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param ArticleCategory $category
     * @return self
     */
    public function removeCategory(ArticleCategory $category): self
    {
        $this->categories->removeElement($category);

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return Collection
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    /**
     * Undocumented function
     *
     * @param ArticleComment $comment
     * @return self
     */
    public function addComment(ArticleComment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setArticle($this);
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param ArticleComment $comment
     * @return self
     */
    public function removeComment(ArticleComment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getArticle() === $this) {
                $comment->setArticle(null);
            }
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->title;
    }
}
