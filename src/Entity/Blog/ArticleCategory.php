<?php

namespace App\Entity\Blog;

use DateTimeImmutable;
use App\Entity\Blog\Article;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use App\Repository\Blog\ArticleCategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\EntityListeners({"App\EntityListener\Blog\ArticleCategoryListener"})
 * @ORM\Entity(repositoryClass=ArticleCategoryRepository::class)
 */
#[UniqueEntity('slug')]
class ArticleCategory
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 50)]
    private string $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotNull()]
    #[Assert\Length(max: 255)]
    private string $slug = 'le-slug';

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    #[Assert\NotNull()]
    private DateTimeImmutable $createdAt;

    /**
     * @ORM\ManyToMany(targetEntity=Article::class, mappedBy="categories")
     */
    private Collection $articles;

    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->articles = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
    }

    /**
     * Undocumented function
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Undocumented function
     *
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * Undocumented function
     *
     * @param string $slug
     * @return self
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return DatetimeImmutable|null
     */
    public function getCreatedAt(): ?DatetimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * Undocumented function
     *
     * @param DatetimeImmutable $createdAt
     * @return self
     */
    public function setCreatedAt(DatetimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return Collection
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    /**
     * Undocumented function
     *
     * @param Article $article
     * @return self
     */
    public function addArticle(Article $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->addCategory($this);
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param Article $article
     * @return self
     */
    public function removeArticle(Article $article): self
    {
        if ($this->articles->removeElement($article)) {
            $article->removeCategory($this);
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }
}
