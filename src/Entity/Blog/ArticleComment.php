<?php

namespace App\Entity\Blog;

use App\Entity\User;
use DateTimeImmutable;
use App\Entity\Blog\Article;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\Blog\ArticleCommentRepository;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ArticleCommentRepository::class)
 */
class ArticleComment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="text")
     */
    #[Assert\NotBlank()]
    #[Assert\Length(min: 1, max: 800)]
    private string $content;

    /**
     * @ORM\Column(type="boolean")
     */
    #[Assert\NotNull()]
    private bool $isApproved = false;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    #[Assert\Length(min: 1, max: 800)]
    private ?string $response;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=Article::class, inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     */
    private Article $article;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="comments")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private User $user;

    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    /**
     * Undocumented function
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * Undocumented function
     *
     * @param string $content
     * @return self
     */
    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return boolean|null
     */
    public function getIsApproved(): ?bool
    {
        return $this->isApproved;
    }

    /**
     * Undocumented function
     *
     * @param boolean $isApproved
     * @return self
     */
    public function setIsApproved(bool $isApproved): self
    {
        $this->isApproved = $isApproved;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getResponse(): ?string
    {
        return $this->response;
    }


    /**
     * Undocumented function
     *
     * @param string|null $response
     * @return self
     */
    public function setResponse(?string $response): self
    {
        $this->response = $response;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return DatetimeImmutable|null
     */
    public function getCreatedAt(): ?DatetimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * Undocumented function
     *
     * @param DatetimeImmutable $createdAt
     * @return self
     */
    public function setCreatedAt(DatetimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return Article|null
     */
    public function getArticle(): ?Article
    {
        return $this->article;
    }

    /**
     * Undocumented function
     *
     * @param Article|null $article
     * @return self
     */
    public function setArticle(?Article $article): self
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * Undocumented function
     *
     * @param User|null $user
     * @return self
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string
     */
    public function __toString()
    {
        return 'Commentaire #' . $this->id;
    }
}
