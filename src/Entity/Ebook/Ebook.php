<?php

namespace App\Entity\Ebook;

use App\Entity\Image;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Ebook\EbookChapter;
use App\Entity\Ebook\EbookOpinion;
use Symfony\Component\HttpFoundation\File\File;
use App\Repository\Ebook\EbookRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\EntityListeners({"App\EntityListener\EbookListener"})
 * @ORM\Entity(repositoryClass=EbookRepository::class)
 * @Vich\Uploadable
 */
#[UniqueEntity('slug')]
class Ebook
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 255)]
    private string $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 255)]
    private string $subtitle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotNull()]
    #[Assert\Length(min: 2, max: 255)]
    private string $slug = 'le-slug';

    /**
     * @ORM\Column(type="text")
     */
    #[Assert\NotBlank()]
    #[Assert\Length(min: 1)]
    private string $introduction;

    /**
     * @ORM\Column(type="text")
     */
    #[Assert\NotBlank()]
    #[Assert\Length(min: 1,)]
    private string $description;

    /**
     * @ORM\Column(type="boolean")
     */
    #[Assert\NotNull()]
    private bool $isPublished = false;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    #[Assert\NotNull()]
    private \DateTimeImmutable $createdAt;

    /**
     * @Vich\UploadableField(mapping="ebook_images", fileNameProperty="imageName")
     */
    private ?File $imageFile = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $imageName;

    /**
     * @ORM\OneToMany(targetEntity=EbookChapter::class, mappedBy="ebook", orphanRemoval=true)
     */
    private Collection $chapters;

    /**
     * @ORM\OneToMany(targetEntity=EbookOpinion::class, mappedBy="ebook", orphanRemoval=true)
     */
    private Collection $opinions;

    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->chapters = new ArrayCollection();
        $this->opinions = new ArrayCollection();
    }

    /**
     * Undocumented function
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Undocumented function
     *
     * @param string $title
     * @return self
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    /**
     * Undocumented function
     *
     * @param string $subtitle
     * @return self
     */
    public function setSubtitle(string $subtitle): self
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * Undocumented function
     *
     * @param string $slug
     * @return self
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getIntroduction(): ?string
    {
        return $this->introduction;
    }

    /**
     * Undocumented function
     *
     * @param string $introduction
     * @return self
     */
    public function setIntroduction(string $introduction): self
    {
        $this->introduction = $introduction;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Undocumented function
     *
     * @param string $description
     * @return self
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return boolean|null
     */
    public function getIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    /**
     * Undocumented function
     *
     * @param boolean $isPublished
     * @return self
     */
    public function setIsPublished(bool $isPublished): self
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return \DateTimeImmutable|null
     */
    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * Undocumented function
     *
     * @param \DateTimeImmutable $createdAt
     * @return self
     */
    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param File|null $imageFile
     * @return void
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->createdAt = new \DateTimeImmutable();
        }
    }

    /**
     * Undocumented function
     *
     * @return File|null
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * Undocumented function
     *
     * @param string|null $imageName
     * @return void
     */
    public function setImageName(?string $imageName): void
    {
        $this->imageName = $imageName;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    /**
     * Undocumented function
     *
     * @return Collection
     */
    public function getChapters(): Collection
    {
        return $this->chapters;
    }

    /**
     * Undocumented function
     *
     * @param EbookChapter $chapter
     * @return self
     */
    public function addChapter(EbookChapter $chapter): self
    {
        if (!$this->chapters->contains($chapter)) {
            $this->chapters[] = $chapter;
            $chapter->setEbook($this);
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param EbookChapter $chapter
     * @return self
     */
    public function removeChapter(EbookChapter $chapter): self
    {
        if ($this->chapters->removeElement($chapter)) {
            // set the owning side to null (unless already changed)
            if ($chapter->getEbook() === $this) {
                $chapter->setEbook(null);
            }
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return Collection
     */
    public function getOpinions(): Collection
    {
        return $this->opinions;
    }

    /**
     * Undocumented function
     *
     * @param EbookOpinion $opinion
     * @return self
     */
    public function addOpinion(EbookOpinion $opinion): self
    {
        if (!$this->opinions->contains($opinion)) {
            $this->opinions[] = $opinion;
            $opinion->setEbook($this);
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param EbookOpinion $opinion
     * @return self
     */
    public function removeOpinion(EbookOpinion $opinion): self
    {
        if ($this->opinions->removeElement($opinion)) {
            // set the owning side to null (unless already changed)
            if ($opinion->getEbook() === $this) {
                $opinion->setEbook(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->title;
    }
}
