<?php

namespace App\Entity\Ebook;

use App\Entity\Ebook\Ebook;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\Ebook\EbookChapterRepository;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=EbookChapterRepository::class)
 */
class EbookChapter
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotBlank()]
    #[Assert\Type('string')]
    #[Assert\Length(min: 2, max: 255)]
    private string $title;

    /**
     * @ORM\Column(type="text")
     */
    #[Assert\NotNull()]
    #[Assert\Length(min: 1,)]
    private string $content;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    #[Assert\NotNull()]
    private \DateTimeImmutable $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=Ebook::class, inversedBy="chapters")
     * @ORM\JoinColumn(nullable=false)
     */
    private Ebook $ebook;

    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    /**
     * Undocumented function
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Undocumented function
     *
     * @param string $title
     * @return self
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * Undocumented function
     *
     * @param string $content
     * @return self
     */
    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return \DateTimeImmutable|null
     */
    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * Undocumented function
     *
     * @param \DateTimeImmutable $createdAt
     * @return self
     */
    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return Ebook|null
     */
    public function getEbook(): ?Ebook
    {
        return $this->ebook;
    }

    /**
     * Undocumented function
     *
     * @param Ebook|null $ebook
     * @return self
     */
    public function setEbook(?Ebook $ebook): self
    {
        $this->ebook = $ebook;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string
     */
    public function __toString()
    {
        return 'Chapitre #' . $this->id;
    }
}
