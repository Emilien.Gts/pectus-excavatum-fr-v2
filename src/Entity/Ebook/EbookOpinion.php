<?php

namespace App\Entity\Ebook;

use App\Entity\Ebook\Ebook;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\Ebook\EbookOpinionRepository;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=EbookOpinionRepository::class)
 */
class EbookOpinion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 255)]
    private string $fullName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 255)]
    private string $city;

    /**
     * @ORM\Column(type="text")
     */
    #[Assert\NotBlank()]
    #[Assert\Length(min: 1)]
    private string $opinion;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    #[Assert\NotNull()]
    private \DateTimeImmutable $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=Ebook::class, inversedBy="opinions")
     * @ORM\JoinColumn(nullable=false)
     */
    private Ebook $ebook;

    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    /**
     * Undocumented function
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    /**
     * Undocumented function
     *
     * @param string $fullName
     * @return self
     */
    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * Undocumented function
     *
     * @param string $city
     * @return self
     */
    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getOpinion(): ?string
    {
        return $this->opinion;
    }

    /**
     * Undocumented function
     *
     * @param string $opinion
     * @return self
     */
    public function setOpinion(string $opinion): self
    {
        $this->opinion = $opinion;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return \DateTimeImmutable|null
     */
    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * Undocumented function
     *
     * @param \DateTimeImmutable $createdAt
     * @return self
     */
    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return Ebook|null
     */
    public function getEbook(): ?Ebook
    {
        return $this->ebook;
    }

    /**
     * Undocumented function
     *
     * @param Ebook|null $ebook
     * @return self
     */
    public function setEbook(?Ebook $ebook): self
    {
        $this->ebook = $ebook;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string
     */
    public function __toString()
    {
        return 'Avis #' . $this->id;
    }
}
