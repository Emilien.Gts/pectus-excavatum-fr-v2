<?php

namespace App\Entity\Faq;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\Faq\FaqRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\EntityListeners({"App\EntityListener\Faq\FaqListener"})
 * @ORM\Entity(repositoryClass=FaqRepository::class)
 */
#[UniqueEntity('question')]
class Faq
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 255)]
    private string $question;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 255)]
    private string $slug;

    /**
     * @ORM\Column(type="text")
     */
    #[Assert\NotBlank()]
    private string $response;

    /**
     * @ORM\Column(type="boolean")
     */
    #[Assert\NotNull()]
    private bool $isPublished;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?DateTimeImmutable $publishedAt;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    #[Assert\NotNull()]
    private DateTimeImmutable $createdAt;

    /**
     * @ORM\ManyToMany(targetEntity=FaqCategory::class, inversedBy="questions")
     */
    private Collection $categories;

    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->createdAt = new DateTimeImmutable();
        $this->faqCategories = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    /**
     * Undocumented function
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getQuestion(): ?string
    {
        return $this->question;
    }

    /**
     * Undocumented function
     *
     * @param string $question
     * @return self
     */
    public function setQuestion(string $question): self
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * Undocumented function
     *
     * @param string $slug
     * @return self
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getResponse(): ?string
    {
        return $this->response;
    }

    /**
     * Undocumented function
     *
     * @param string $response
     * @return self
     */
    public function setResponse(string $response): self
    {
        $this->response = $response;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return boolean|null
     */
    public function getIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    /**
     * Undocumented function
     *
     * @param boolean $isPublished
     * @return self
     */
    public function setIsPublished(bool $isPublished): self
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return \DateTimeImmutable|null
     */
    public function getPublishedAt(): ?\DateTimeImmutable
    {
        return $this->publishedAt;
    }

    /**
     * Undocumented function
     *
     * @param \DateTimeImmutable|null $publishedAt
     * @return self
     */
    public function setPublishedAt(?\DateTimeImmutable $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return \DateTimeImmutable|null
     */
    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * Undocumented function
     *
     * @param \DateTimeImmutable $createdAt
     * @return self
     */
    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return Collection
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    /**
     * Undocumented function
     *
     * @param FaqCategory $category
     * @return self
     */
    public function addCategory(FaqCategory $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param FaqCategory $category
     * @return self
     */
    public function removeCategory(FaqCategory $category): self
    {
        $this->categories->removeElement($category);

        return $this;
    }
}
