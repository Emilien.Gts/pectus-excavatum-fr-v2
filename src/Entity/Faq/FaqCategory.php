<?php

namespace App\Entity\Faq;

use App\Repository\Faq\FaqCategoryRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\EntityListeners({"App\EntityListener\Faq\FaqCategoryListener"})
 * @ORM\Entity(repositoryClass=FaqCategoryRepository::class)
 * @Vich\Uploadable
 */
#[UniqueEntity('name')]
class FaqCategory
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 255)]
    private string $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 255)]
    private string $slug;

    /**
     * @Vich\UploadableField(mapping="faq_images", fileNameProperty="imageName")
     */
    private ?File $imageFile = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $imageName;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    #[Assert\NotNull()]
    private DateTimeImmutable $createdAt;

    /**
     * @ORM\ManyToMany(targetEntity=Faq::class, mappedBy="categories")
     */
    private Collection $questions;

    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->createdAt = new DateTimeImmutable();
        $this->questions = new ArrayCollection();
    }

    /**
     * Undocumented function
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Undocumented function
     *
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * Undocumented function
     *
     * @param string $slug
     * @return self
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param File|null $imageFile
     * @return void
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->createdAt = new DateTimeImmutable();
        }
    }

    /**
     * Undocumented function
     *
     * @return File|null
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * Undocumented function
     *
     * @param string|null $imageName
     * @return void
     */
    public function setImageName(?string $imageName): void
    {
        $this->imageName = $imageName;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    /**
     * Undocumented function
     *
     * @return \DateTimeImmutable|null
     */
    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * Undocumented function
     *
     * @param \DateTimeImmutable $createdAt
     * @return self
     */
    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return Collection
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    /**
     * Undocumented function
     *
     * @param Faq $question
     * @return self
     */
    public function addQuestion(Faq $question): self
    {
        if (!$this->questions->contains($question)) {
            $this->questions[] = $question;
            $question->addCategory($this);
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param Faq $question
     * @return self
     */
    public function removeQuestion(Faq $question): self
    {
        if ($this->questions->removeElement($question)) {
            $question->removeCategory($this);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}
