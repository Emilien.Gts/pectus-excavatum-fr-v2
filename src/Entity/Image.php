<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ImageRepository;
use Cocur\Slugify\Slugify;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\EntityListeners({"App\EntityListener\ImageListener"})
 * @ORM\Entity(repositoryClass=ImageRepository::class)
 */
#[UniqueEntity('name')]
class Image
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotBlank(
        message: 'Le nom de l\'image ne peut pas être vide.'
    )]
    #[Assert\Type('string')]
    #[Assert\Length(
        min: 1,
        max: 255,
        minMessage: 'Le nom de l\'image doit comporter au moins {{ limit }} caractères.',
        maxMessage: 'Le nom de l\'image doit comporter au plus {{ limit }} caractères.',
    )]
    private string $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotBlank(
        message: 'Le texte alternatif de l\'image ne peut pas être vide.'
    )]
    #[Assert\Type('string')]
    #[Assert\Length(
        min: 1,
        max: 255,
        minMessage: 'Le texte alternatif de l\'image doit comporter au moins {{ limit }} caractères.',
        maxMessage: 'Le texte alternatif de l\'image doit comporter au plus {{ limit }} caractères.',
    )]
    private string $alt;

    /**
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAlt(): ?string
    {
        return $this->alt;
    }

    /**
     * @param string $alt
     * @return self
     */
    public function setAlt(string $alt): self
    {
        $this->alt = $alt;

        return $this;
    }
}
