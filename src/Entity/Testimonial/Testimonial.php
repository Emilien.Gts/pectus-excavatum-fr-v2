<?php

namespace App\Entity\Testimonial;

use App\Entity\Image;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use App\Entity\Testimonial\TestimonialComment;
use Doctrine\Common\Collections\ArrayCollection;
use App\Repository\Testimonial\TestimonialRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\EntityListeners({"App\EntityListener\Testimonial\TestimonialListener"})
 * @ORM\Entity(repositoryClass=TestimonialRepository::class)
 * @Vich\Uploadable
 */
#[UniqueEntity('slug')]
class Testimonial
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 255)]
    private string $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 255)]
    private string $slug;

    /**
     * @ORM\Column(type="integer")
     */
    #[Assert\NotNull()]
    #[Assert\Positive(message: 'Le temps de lecture ne peut pas être inférieur ou égale à zéro.')]
    private int $readingTime;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 255)]
    private string $fullName;

    /**
     * @ORM\Column(type="boolean")
     */
    #[Assert\NotNull()]
    private bool $isMan;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 255)]
    private string $city;

    /**
     * @ORM\Column(type="text")
     */
    #[Assert\NotBlank()]
    private string $testimonial;

    /**
     * @ORM\Column(type="boolean")
     */
    #[Assert\NotNull()]
    private bool $isPublished;

    /**
     * @Vich\UploadableField(mapping="testimonial_images", fileNameProperty="imageName")
     */
    private ?File $imageFile = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $imageName;

    /**
     * @ORM\Column(type="datetime")
     */
    #[Assert\NotNull()]
    private \DateTime $createdAt;

    /**
     * @ORM\OneToMany(targetEntity=TestimonialComment::class, mappedBy="testimonial")
     */
    private Collection $comments;

    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->comments = new ArrayCollection();
    }

    /**
     * Undocumented function
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Undocumented function
     *
     * @param string $title
     * @return self
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * Undocumented function
     *
     * @param string $slug
     * @return self
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return integer|null
     */
    public function getReadingTime(): ?int
    {
        return $this->readingTime;
    }

    /**
     * Undocumented function
     *
     * @param integer $readingTime
     * @return self
     */
    public function setReadingTime(int $readingTime): self
    {
        $this->readingTime = $readingTime;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    /**
     * Undocumented function
     *
     * @param string $fullName
     * @return self
     */
    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return boolean|null
     */
    public function getIsMan(): ?bool
    {
        return $this->isMan;
    }

    /**
     * Undocumented function
     *
     * @param boolean $isMan
     * @return self
     */
    public function setIsMan(bool $isMan): self
    {
        $this->isMan = $isMan;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * Undocumented function
     *
     * @param string $city
     * @return self
     */
    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getTestimonial(): ?string
    {
        return $this->testimonial;
    }

    /**
     * Undocumented function
     *
     * @param string $testimonial
     * @return self
     */
    public function setTestimonial(string $testimonial): self
    {
        $this->testimonial = $testimonial;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return boolean|null
     */
    public function getIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    /**
     * Undocumented function
     *
     * @param boolean $isPublished
     * @return self
     */
    public function setIsPublished(bool $isPublished): self
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param File|null $imageFile
     * @return void
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            // $this->createdAt = new \DateTime();
        }
    }

    /**
     * Undocumented function
     *
     * @return File|null
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * Undocumented function
     *
     * @param string|null $imageName
     * @return void
     */
    public function setImageName(?string $imageName): void
    {
        $this->imageName = $imageName;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    /**
     * Undocumented function
     *
     * @return \DateTimeInterface|null
     */
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * Undocumented function
     *
     * @param \DateTimeInterface $createdAt
     * @return self
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return Image|null
     */
    public function getThumbnail(): ?Image
    {
        return $this->thumbnail;
    }

    /**
     * Undocumented function
     *
     * @param Image|null $thumbnail
     * @return self
     */
    public function setThumbnail(?Image $thumbnail): self
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return Collection
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    /**
     * Undocumented function
     *
     * @param TestimonialComment $comment
     * @return self
     */
    public function addComment(TestimonialComment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setTestimonial($this);
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param TestimonialComment $comment
     * @return self
     */
    public function removeComment(TestimonialComment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getTestimonial() === $this) {
                $comment->setTestimonial(null);
            }
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string
     */
    public function __toString()
    {
        return $this->title;
    }
}
