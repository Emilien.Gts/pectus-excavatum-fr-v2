<?php

namespace App\Entity\Testimonial;

use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Testimonial\Testimonial;
use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\Testimonial\TestimonialCommentRepository;

/**
 * @ORM\Entity(repositoryClass=TestimonialCommentRepository::class)
 */
class TestimonialComment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="text")
     */
    #[Assert\NotBlank()]
    #[Assert\Length(min: 1, max: 800)]
    private string $content;

    /**
     * @ORM\Column(type="boolean")
     */
    #[Assert\NotNull()]
    private bool $isApproved = false;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    #[Assert\Length(min: 1, max: 800)]
    private ?string $response;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTime $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="testimonialComments")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private user $user;

    /**
     * @ORM\ManyToOne(targetEntity=Testimonial::class, inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     */
    private Testimonial $testimonial;

    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Undocumented function
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * Undocumented function
     *
     * @param string $content
     * @return self
     */
    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return boolean|null
     */
    public function getIsApproved(): ?bool
    {
        return $this->isApproved;
    }

    /**
     * Undocumented function
     *
     * @param boolean $isApproved
     * @return self
     */
    public function setIsApproved(bool $isApproved): self
    {
        $this->isApproved = $isApproved;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getResponse(): ?string
    {
        return $this->response;
    }

    /**
     * Undocumented function
     *
     * @param string|null $response
     * @return self
     */
    public function setResponse(?string $response): self
    {
        $this->response = $response;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return \DateTimeInterface|null
     */
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * Undocumented function
     *
     * @param \DateTimeInterface $createdAt
     * @return self
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * Undocumented function
     *
     * @param User|null $user
     * @return self
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return Testimonial|null
     */
    public function getTestimonial(): ?Testimonial
    {
        return $this->testimonial;
    }

    /**
     * Undocumented function
     *
     * @param Testimonial|null $testimonial
     * @return self
     */
    public function setTestimonial(?Testimonial $testimonial): self
    {
        $this->testimonial = $testimonial;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string
     */
    public function __toString()
    {
        return 'Commentaire #' . $this->id;
    }
}
