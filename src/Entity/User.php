<?php

namespace App\Entity;

use App\Entity\Blog\ArticleComment;
use App\Entity\Testimonial\TestimonialComment;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\EntityListeners({"App\EntityListener\UserListener"})
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
#[UniqueEntity('email')]
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    #[Assert\NotBlank(
        message: 'Ce champ ne peut pas être laissé vide.'
    )]
    #[Assert\Type('string')]
    #[Assert\Length(
        min: 2,
        max: 50,
        minMessage: 'Votre prénom doit comporter au moins {{ limit }} caractères.',
        maxMessage: 'Votre prénom doit comporter au plus {{ limit }} caractères.',
    )]
    private string $firstName;

    /**
     * @ORM\Column(type="string", length=50)
     */
    #[Assert\NotBlank(
        message: 'Ce champ ne peut pas être laissé vide.'
    )]
    #[Assert\Type('string')]
    #[Assert\Length(
        min: 2,
        max: 50,
        minMessage: 'Votre nom doit comporter au moins {{ limit }} caractères.',
        maxMessage: 'Votre nom doit comporter au plus {{ limit }} caractères.',
    )]
    private string $lastName;

    /**
     * @ORM\Column(type="string", length=50)
     */
    #[Assert\Type('string')]
    #[Assert\Length(
        max: 50,
        maxMessage: 'Votre pseduo doit comporter au plus {{ limit }} caractères.',
    )]
    private string $pseudo;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    #[Assert\NotBlank(
        message: 'Ce champ ne peut pas être laissé vide.'
    )]
    #[Assert\Type('string')]
    #[Assert\Email(
        message: 'L\'email {{ value }} n\'est pas un email valide.',
    )]
    private string $email;

    /**
     * @ORM\Column(type="json")
     */
    #[Assert\NotBlank]
    private array $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private ?string $password;

    private ?string $plainPassword = null;

    /**
     * @ORM\Column(type="datetime")
     */
    #[Assert\Type('datetime')]
    #[Assert\NotNull]
    private \DateTime $createdAt;

    /**
     * @ORM\OneToMany(targetEntity=ArticleComment::class, mappedBy="user")
     */
    private Collection $comments;

    /**
     * @ORM\OneToMany(targetEntity=TestimonialComment::class, mappedBy="user")
     */
    private Collection $testimonialComments;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $forgottenPasswordToken;

    /**
     * @ORM\Column(type="boolean")
     */
    private $agreeTerms;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $deletedAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt = new \Datetime();
        $this->deletedAt = (new \DateTimeImmutable())->add(new \DateInterval('P36M'));
        $this->roles[] = 'ROLE_USER';
        $this->comments = new ArrayCollection();
        $this->testimonialComments = new ArrayCollection();
    }

    /**
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return self
     */
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return self
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    /**
     * @param string $pseudo
     * @return self
     */
    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return self
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @param array $roles
     * @return self
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(?string $plainPassword): void
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTimeInterface $createdAt
     * @return self
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection|ArticleComment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(ArticleComment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setUser($this);
        }

        return $this;
    }

    public function removeComment(ArticleComment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getUser() === $this) {
                $comment->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TestimonialComment[]
     */
    public function getTestimonialComments(): Collection
    {
        return $this->testimonialComments;
    }

    public function addTestimonialComment(TestimonialComment $testimonialComment): self
    {
        if (!$this->testimonialComments->contains($testimonialComment)) {
            $this->testimonialComments[] = $testimonialComment;
            $testimonialComment->setUser($this);
        }

        return $this;
    }

    public function removeTestimonialComment(TestimonialComment $testimonialComment): self
    {
        if ($this->testimonialComments->removeElement($testimonialComment)) {
            // set the owning side to null (unless already changed)
            if ($testimonialComment->getUser() === $this) {
                $testimonialComment->setUser(null);
            }
        }

        return $this;
    }

    public function getForgottenPasswordToken(): ?string
    {
        return $this->forgottenPasswordToken;
    }

    public function setForgottenPasswordToken(?string $forgottenPasswordToken): self
    {
        $this->forgottenPasswordToken = $forgottenPasswordToken;

        return $this;
    }

    public function __toString()
    {
        return
            $this->firstName . ' ' .
            (($this->pseudo !== null || $this->pseudo !== '') ? ('(' . $this->pseudo . ') ') : '') .
            strtoupper($this->lastName);
    }

    public function getAgreeTerms(): ?bool
    {
        return $this->agreeTerms;
    }

    public function setAgreeTerms(bool $agreeTerms): self
    {
        $this->agreeTerms = $agreeTerms;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(\DateTimeImmutable $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }
}
