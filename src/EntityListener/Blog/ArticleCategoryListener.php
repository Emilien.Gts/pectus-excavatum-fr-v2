<?php

declare(strict_types=1);

namespace App\EntityListener\Blog;

use Cocur\Slugify\Slugify;
use App\Entity\Blog\ArticleCategory;

class ArticleCategoryListener
{
    /**
     * @var Slugify
     */
    private Slugify $slugify;

    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->slugify = new Slugify();
    }

    /**
     * Undocumented function
     *
     * @param ArticleCategory $category
     * @return void
     */
    public function prePersist(ArticleCategory $category): void
    {
        $this->setSlug($category);
    }

    /**
     * Undocumented function
     *
     * @param ArticleCategory $category
     * @return void
     */
    private function setSlug(ArticleCategory $category): void
    {
        if ($category->getSlug() === 'le-slug') {
            $category->setSlug(
                $this->slugify->slugify($category->getName())
            );
        }
    }
}
