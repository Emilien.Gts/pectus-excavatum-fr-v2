<?php

declare(strict_types=1);

namespace App\EntityListener\Blog;

use Cocur\Slugify\Slugify;
use App\Entity\Blog\Article;

class ArticleListener
{
    /**
     * @var Slugify
     */
    private Slugify $slugify;

    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->slugify = new Slugify();
    }

    /**
     * Undocumented function
     *
     * @param Article $article
     * @return void
     */
    public function prePersist(Article $article): void
    {
        $this->setSlug($article);
        $this->setReadingTime($article);
    }

    /**
     * Undocumented function
     *
     * @param Article $article
     * @return void
     */
    public function preUpdate(Article $article): void
    {
        $this->setReadingTime($article);
    }

    /**
     * Undocumented function
     *
     * @param Article $article
     * @return void
     */
    private function setSlug(Article $article): void
    {
        if ($article->getSlug() === 'le-slug') {
            $article->setSlug(
                $this->slugify->slugify($article->getTitle())
            );
        }
    }

    /**
     * Undocumented function
     *
     * @param Article $article
     * @return void
     */
    private function setReadingTime(Article $article): void
    {
        $article->setReadingTime($this->readingTime($article->getContent()));
    }

    /**
     * Defines reding time based on content
     *
     * @param string $content
     * @return integer
     */
    private function readingTime(string $content): int
    {
        // Predefined words-per-minute rate.
        $words_per_minute = 225;
        $words_per_second = $words_per_minute / 60;

        // Count the words in the content.
        $word_count = str_word_count(strip_tags($content));

        // How many seconds (total)?
        $seconds_total = floor($word_count / $words_per_second);
        $result = (int) ceil($seconds_total / 60);

        return $result <= 0 ? 1 : $result;
    }
}
