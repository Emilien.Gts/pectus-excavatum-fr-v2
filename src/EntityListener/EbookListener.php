<?php

declare(strict_types=1);

namespace App\EntityListener;

use Cocur\Slugify\Slugify;
use App\Entity\Ebook\Ebook;

class EbookListener
{
    /**
     * @var Slugify
     */
    private Slugify $slugify;

    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->slugify = new Slugify();
    }

    /**
     * Undocumented function
     *
     * @param Ebook $ebook
     * @return void
     */
    public function prePersist(Ebook $ebook): void
    {
        $this->setSlug($ebook);
    }

    /**
     * Undocumented function
     *
     * @param Ebook $ebook
     * @return void
     */
    private function setSlug(Ebook $ebook): void
    {
        if ($ebook->getSlug() === 'le-slug') {
            $ebook->setSlug(
                $this->slugify->slugify($ebook->getTitle())
            );
        }
    }
}
