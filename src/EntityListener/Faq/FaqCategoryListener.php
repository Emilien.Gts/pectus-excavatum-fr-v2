<?php

declare(strict_types=1);

namespace App\EntityListener\Faq;

use Cocur\Slugify\Slugify;
use App\Entity\Faq\FaqCategory;

class FaqCategoryListener
{
    private Slugify $slugify;

    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->slugify = new Slugify();
    }

    /**
     * Undocumented function
     *
     * @param FaqCategory $category
     * @return void
     */
    public function prePersist(FaqCategory $category): void
    {
        $this->setSlug($category);
    }

    /**
     * Undocumented function
     *
     * @param FaqCategory $category
     * @return void
     */
    private function setSlug(FaqCategory $category): void
    {
        $category->setSlug(
            $this->slugify->slugify($category->getName())
        );
    }
}