<?php

declare(strict_types=1);

namespace App\EntityListener\Faq;

use Cocur\Slugify\Slugify;
use App\Entity\Faq\Faq;
use DateTimeImmutable;

class FaqListener
{
    private Slugify $slugify;

    public function __construct()
    {
        $this->slugify = new Slugify();
    }

    public function prePersist(Faq $faq): void
    {
        $this->setSlug($faq);
        if($faq->getIsPublished()) {
            $faq->setPublishedAt(new DateTimeImmutable());
        }
    }

    public function preUpdate(Faq $faq): void
    {
        if($faq->getIsPublished()) {
            $faq->setPublishedAt(new DateTimeImmutable());
        }
    }

    private function setSlug(Faq $faq): void
    {
        $faq->setSlug(
            $this->slugify->slugify($faq->getQuestion())
        );
    }
}