<?php

declare(strict_types=1);

namespace App\EntityListener;

use Cocur\Slugify\Slugify;
use App\Entity\Image;

class ImageListener
{
    private Slugify $slugify;

    public function __construct()
    {
        $this->slugify = new Slugify();
    }

    public function prePersist(Image $image): void
    {
        $this->setSlug($image);
    }

    private function setSlug(Image $image): void
    {
        $image->setAlt(
            $this->slugify->slugify($image->getName())
        );
    }
}