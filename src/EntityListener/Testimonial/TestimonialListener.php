<?php

declare(strict_types=1);

namespace App\EntityListener\Testimonial;

use Cocur\Slugify\Slugify;
use App\Entity\Testimonial\Testimonial;

class TestimonialListener
{
    /**
     * @var Slugify
     */
    private Slugify $slugify;

    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->slugify = new Slugify();
    }

    /**
     * Undocumented function
     *
     * @param Testimonial $testimonial
     * @return void
     */
    public function prePersist(Testimonial $testimonial): void
    {
        $this->setSlug($testimonial);
        $this->setReadingTime($testimonial);
    }

    /**
     * Undocumented function
     *
     * @param Testimonial $testimonial
     * @return void
     */
    public function preUpdate(Testimonial $testimonial): void
    {
        $this->setReadingTime($testimonial);
    }

    /**
     * Undocumented function
     *
     * @param Testimonial $testimonial
     * @return void
     */
    private function setSlug(Testimonial $testimonial): void
    {
        $testimonial->setSlug(
            $this->slugify->slugify($testimonial->getTitle())
        );
    }

    /**
     * Undocumented function
     *
     * @param Testimonial $testimonial
     * @return void
     */
    private function setReadingTime(Testimonial $testimonial): void
    {
        $testimonial->setReadingTime($this->readingTime($testimonial->getTestimonial()));
    }

    /**
     * Defines reding time based on content
     *
     * @param string $content
     * @return integer
     */
    private function readingTime(string $content): int
    {
        // Predefined words-per-minute rate.
        $words_per_minute = 225;
        $words_per_second = $words_per_minute / 60;

        // Count the words in the content.
        $word_count = str_word_count(strip_tags($content));

        // How many seconds (total)?
        $seconds_total = floor($word_count / $words_per_second);
        $result = (int) ceil($seconds_total / 60);

        return $result <= 0 ? 1 : $result;
    }
}
