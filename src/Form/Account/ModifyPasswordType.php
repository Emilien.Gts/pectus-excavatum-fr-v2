<?php

namespace App\Form\Account;

use App\Model\ModifyPassword;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\NotBlank;

class ModifyPasswordType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('plainPassword', PasswordType::class, [
                'label' => 'Nouveau mot de passe',
                'label_attr' => ['class' => 'comment__label'],
                'attr' => ['class' => 'comment__input'],
                'constraints' => [new NotBlank()]
            ])
            ->add('repeatedPassword', PasswordType::class, [
                'label' => 'Confirmation du mot de passe',
                'label_attr' => ['class' => 'comment__label'],
                'attr' => ['class' => 'comment__input'],
                'constraints' => [new NotBlank()]
            ])
            ->add('actualPassword', PasswordType::class, [
                'label' => 'Mot de passe actuel',
                'label_attr' => ['class' => 'comment__label'],
                'attr' => ['class' => 'comment__input'],
                'constraints' => [new NotBlank()]
            ]);
    }
}
