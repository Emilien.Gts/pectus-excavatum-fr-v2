<?php

namespace App\Form\Authentication;

use App\Entity\User;
use Doctrine\DBAL\Types\BooleanType;
use Symfony\Component\Form\AbstractType;
use Gregwar\CaptchaBundle\Type\CaptchaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class RegistrationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, [
                'attr' => [
                    'min' => 2,
                    'max' => 50,
                    'placeholder' => 'Emilien'
                ],
                'label' => 'Prénom'
            ])
            ->add('lastName', TextType::class, [
                'attr' => [
                    'min' => 2,
                    'max' => 50,
                    'placeholder' => 'Gts'
                ],
                'label' => 'Nom'
            ])
            ->add('pseudo', TextType::class, [
                'attr' => [
                    'max' => 50,
                    'placeholder' => 'EmilienGts'
                ],
                'empty_data' => '',
                'label' => 'Pseudo (Optionnel)',
                'required' => false
            ])
            ->add('email', EmailType::class, [
                'attr' => [
                    'placeholder' => 'emilien@pectusexcavatum.fr'
                ]
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options'  => [
                    'label' => 'Mot de passe',
                    'attr' => [
                        'placeholder' => '****************',
                    ]
                ],
                'second_options' => [
                    'label' => 'Confirmations du mot de passe',
                    'attr' => [
                        'placeholder' => '****************',
                    ]
                ],
                'invalid_message' => 'Les mots de passe ne correspondent pas.'
            ])
            ->add('captcha', CaptchaType::class, [
                'label' => '',
            ])
            ->add('agreeTerms', CheckboxType::class, ['required' => 'true']);
    }

    /**
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}
