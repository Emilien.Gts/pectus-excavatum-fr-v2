<?php

namespace App\Form\Ebook;

use App\Model\EbookDownload;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class DownloadType extends AbstractType
{
    /**
     * Undocumented function
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Votre email',
                'label_attr' => ['class' => 'download__label'],
                'attr' => ['class' => 'download__input'],
                'constraints' => [new NotBlank(), new Email()]
            ])
            ->add('pseudo', TextType::class, [
                'label' => 'Votre pseudo',
                'label_attr' => ['class' => 'download__label'],
                'attr' => ['class' => 'download__input'],
                'required' => false
            ])
            ->add('agreeTerms', CheckboxType::class, ['required' => 'true']);
    }
}
