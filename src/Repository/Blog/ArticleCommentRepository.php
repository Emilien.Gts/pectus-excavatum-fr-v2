<?php

namespace App\Repository\Blog;

use App\Entity\Blog\Article;
use App\Entity\Blog\ArticleComment;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method ArticleComment|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArticleComment|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArticleComment[]    findAll()
 * @method ArticleComment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleCommentRepository extends ServiceEntityRepository
{
    /**
     * Undocumented function
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ArticleComment::class);
    }

    /**
     * Find all comments which are published from an article
     *
     * @return array
     */
    public function findComments(Article $currentArticle): array
    {
        return $this->createQueryBuilder('c')
            ->join('c.article', 'a')
            ->where('c.isApproved = 1')
            ->andWhere('a.id = :aId')
            ->setParameter('aId', $currentArticle->getId())
            ->getQuery()
            ->getResult();
    }

    /**
     * Find last comments
     *
     * @return array
     */
    public function findLastComments(int $maxResults): array
    {
        return $this->createQueryBuilder('c')
            ->orderBy('c.createdAt', 'DESC')
            ->setMaxResults($maxResults)
            ->getQuery()
            ->getResult();
    }

    /**
     * Find unapproved comments
     *
     * @return array
     */
    public function findUnapprovedComments(): array
    {
        return $this->createQueryBuilder('c')
            ->where('c.isApproved = 0')
            ->getQuery()
            ->getResult();
    }
}
