<?php

namespace App\Repository\Blog;

use App\Entity\Blog\Article;
use App\Entity\Blog\ArticleCategory;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    /**
     * Constructor
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    /**
     * Find two featured articles
     *
     * @return array
     */
    public function findFeaturedArticles(): array
    {
        return $this->createQueryBuilder('a')
            ->where('a.isForward = 1')
            ->andWhere('a.isPublished = 1')
            ->setMaxResults(2)
            ->getQuery()
            ->getResult();
    }

    /**
     * Find all articles excepts the featured articles
     *
     * @return array
     */
    public function findSimpleArticles(): array
    {
        return $this->createQueryBuilder('a')
            ->where('a.isForward = 0')
            ->andWhere('a.isPublished = 1')
            ->orderBy('a.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Find three similar articles based on first category of the article
     *
     * @param Article $currentArticle
     * @return array
     */
    public function findSimilarArticles(Article $currentArticle): array
    {
        return $this->createQueryBuilder('a')
            ->join('a.categories', 'c')
            ->where('a.isPublished = 1')
            ->andWhere('a.id != :aId')
            ->andWhere('c.id = :cId')
            ->setParameter('aId', $currentArticle->getId())
            ->setParameter('cId', $currentArticle->getCategories()->first()->getId())
            ->orderBy('a.createdAt', 'DESC')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult();
    }

    /**
     * Find articles by a specific category
     *
     * @param ArticleCategory $category
     * @return array
     */
    public function findArticles(ArticleCategory $category): array
    {
        return $this->createQueryBuilder('a')
            ->join('a.categories', 'c')
            ->where('a.isPublished = 1')
            ->andWhere('c.id = :cId')
            ->orderBy('a.createdAt', 'DESC')
            ->setParameter('cId', $category->getId())
            ->getQuery()
            ->getResult();
    }

    /**
     * Find last articles
     *
     * @param integer $maxResults
     * @return array
     */
    public function findLastArticles(int $maxResults): array
    {
        return $this->createQueryBuilder('p')
            ->where('p.isPublished = 1')
            ->orderBy('p.createdAt', 'DESC')
            ->setMaxResults($maxResults)
            ->getQuery()
            ->getResult();
    }
}
