<?php

namespace App\Repository\Ebook;

use App\Entity\Ebook\EbookChapter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EbookChapter|null find($id, $lockMode = null, $lockVersion = null)
 * @method EbookChapter|null findOneBy(array $criteria, array $orderBy = null)
 * @method EbookChapter[]    findAll()
 * @method EbookChapter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EbookChapterRepository extends ServiceEntityRepository
{
    /**
     * Constructor
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EbookChapter::class);
    }
}
