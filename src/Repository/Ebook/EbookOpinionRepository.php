<?php

namespace App\Repository\Ebook;

use App\Entity\Ebook\EbookOpinion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EbookOpinion|null find($id, $lockMode = null, $lockVersion = null)
 * @method EbookOpinion|null findOneBy(array $criteria, array $orderBy = null)
 * @method EbookOpinion[]    findAll()
 * @method EbookOpinion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EbookOpinionRepository extends ServiceEntityRepository
{
    /**
     * Constructor
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EbookOpinion::class);
    }
}
