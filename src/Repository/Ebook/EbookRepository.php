<?php

namespace App\Repository\Ebook;

use App\Entity\Ebook\Ebook;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Ebook|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ebook|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ebook[]    findAll()
 * @method Ebook[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EbookRepository extends ServiceEntityRepository
{
    /**
     * Constructor
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ebook::class);
    }

    /**
     * Find all ebooks wich are published
     *
     * @return array
     */
    public function findEbooks(): array
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.isPublished = 1')
            ->getQuery()
            ->getResult();
    }
}
