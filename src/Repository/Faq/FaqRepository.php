<?php

namespace App\Repository\Faq;

use App\Entity\Faq\Faq;
use App\Entity\Faq\FaqCategory;
use App\Entity\Blog\ArticleCategory;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Faq|null find($id, $lockMode = null, $lockVersion = null)
 * @method Faq|null findOneBy(array $criteria, array $orderBy = null)
 * @method Faq[]    findAll()
 * @method Faq[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FaqRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Faq::class);
    }

    /**
     * Get only published questions on a category
     *
     * @param FaqCategory $category
     * @return void
     */
    public function getPublishedQuestions(FaqCategory $category)
    {
        return $this->createQueryBuilder('f')
            ->join('f.categories', 'c')
            ->where('f.isPublished = 1')
            ->andWhere(':category MEMBER OF f.categories')
            ->setParameter('category', $category)
            ->getQuery()
            ->getResult();
    }

    /**
     * Search some FAQs based on a search
     *
     * @param string $search
     * @return void
     */
    public function getSearch(string $search)
    {
        $query = $this->createQueryBuilder('f')
            ->where('f.isPublished = 1');

        if (!empty($search)) {
            $query = $query
                ->andWhere('f.question LIKE :search OR f.response LIKE :search')
                ->setParameter('search', "%{$search}%");
        }

        return $query = $query->getQuery()
            ->getResult();
    }
}
