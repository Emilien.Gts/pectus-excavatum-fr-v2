<?php

namespace App\Repository\Testimonial;

use App\Entity\Testimonial\Testimonial;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Testimonial\TestimonialComment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method TestimonialComment|null find($id, $lockMode = null, $lockVersion = null)
 * @method TestimonialComment|null findOneBy(array $criteria, array $orderBy = null)
 * @method TestimonialComment[]    findAll()
 * @method TestimonialComment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TestimonialCommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TestimonialComment::class);
    }

    /**
     * Find all comments which are published from a testimonial
     *
     * @return array
     */
    public function findPublishedCommentsFromATestimonial(Testimonial $currentTestimonial): array
    {
        return $this->createQueryBuilder('c')
            ->join('c.testimonial', 't')
            ->where('t.title = :tTitle')
            ->setParameter('tTitle', $currentTestimonial->getTitle())
            ->andWhere('c.isApproved = 1')
            ->getQuery()
            ->getResult();
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function findLastFiveComments()
    {
        return $this->createQueryBuilder('c')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();
    }

    /**
     * Find unapproved comments
     *
     * @return array
     */
    public function findUnapprovedComments(): array
    {
        return $this->createQueryBuilder('c')
            ->where('c.isApproved = 0')
            ->getQuery()
            ->getResult();
    }
}
