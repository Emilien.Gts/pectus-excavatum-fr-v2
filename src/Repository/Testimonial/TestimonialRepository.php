<?php

namespace App\Repository\Testimonial;

use App\Entity\Testimonial\Testimonial;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Testimonial|null find($id, $lockMode = null, $lockVersion = null)
 * @method Testimonial|null findOneBy(array $criteria, array $orderBy = null)
 * @method Testimonial[]    findAll()
 * @method Testimonial[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TestimonialRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Testimonial::class);
    }

    /**
     * Find all testimonials which are published
     *
     * @return array
     */
    public function findSimpleTestimonials(): array
    {
        return $this->createQueryBuilder('t')
            ->where('t.isPublished = 1')
            ->orderBy('t.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Find last testimonials
     *
     * @return array
     */
    public function findLastThreeTestimonials(Testimonial $testimonial): array
    {
        return $this->createQueryBuilder('t')
            ->orderBy('t.createdAt', 'DESC')
            ->where('t.id != :currentId')
            ->setParameter('currentId', $testimonial->getId())
            ->setMaxResults(3)
            ->getQuery()
            ->getResult();
    }

    /**
     * Find last testimonials
     *
     * @return array
     */
    public function findLastTestimonials(): array
    {
        return $this->createQueryBuilder('p')
            ->where('p.isPublished = 1')
            ->orderBy('p.createdAt', 'DESC')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult();
    }
}
