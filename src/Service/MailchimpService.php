<?php

namespace App\Service;

use MailchimpMarketing\ApiClient;
use Symfony\Component\HttpFoundation\Response;

class MailchimpService
{
    private ApiClient $mailchimp;

    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->mailchimp = new ApiClient();
        $this->mailchimp->setConfig([
            'apiKey' => $_ENV["MAILCHIMP_API_KEY"],
            'server' => $_ENV["MAILCHIMP_SERVER_PREFIX"]
        ]);
    }

    /**
     * Set a new Mailchimp's member
     *
     * @param array $data
     * @return void
     */
    public function setNewMember(array $data): void
    {
        $this->mailchimp->lists->setListMember($_ENV["MAILCHIMP_LIST_ID"], strtolower(md5($data['email'])), [
            "email_address" => $data['email'],
            "status_if_new" => "subscribed",
            "merge_fields" => [
                "FNAME" => $data['pseudo']
            ]
        ]);
    }
}
