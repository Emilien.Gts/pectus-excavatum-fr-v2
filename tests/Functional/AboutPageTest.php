<?php

namespace Tests\Functional;

use App\Tests\Functional\FunctionalTest;

class HomePageTest extends FunctionalTest
{
    /**
     * Undocumented function
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    /**
     * Check good HTTP response, h1 and title
     *
     * @return void
     */
    public function testAboutPageIsGood(): void
    {
        $this->pageIsGood(
            '/a-propos',
            'À propos',
            'À propos - Pectus Excavatum FR',
            $this->client
        );
    }
}
