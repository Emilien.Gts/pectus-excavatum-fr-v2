<?php

namespace App\Tests\Functional;

use App\Entity\User;

class DeleteAccountTest extends FunctionalTest
{
    /**
     * Undocumented function
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = $this->client->getContainer()->get("doctrine.orm.entity_manager");
    }

    /**
     * Check good HTTP response, h1 and title
     *
     * @return void
     */
    public function testDeletePageIsGood(): void
    {
        $this->userLogin($this->client);
        $this->pageIsGood(
            '/mon-compte/supprimer',
            'Supprimer mon compte',
            'Supprimer mon compte - Pectus Excavatum FR',
            $this->client
        );
    }

    /**
     * Check good HTTP response, h1 and title
     *
     * @return void
     */
    public function testDeleteConfirmationPageIsGood(): void
    {
        $this->userLogin($this->client);
        $this->pageIsGood(
            '/mon-compte/supprimer-confirmation',
            'Confirmation de suppression du compte',
            'Confirmation de suppression du compte - Pectus Excavatum FR',
            $this->client
        );
    }    

    /**
     * Test good delete of user account
     *
     * @return void
     */
    public function testModifyInformationsFormIfPasswordNotGood(): void
    {
        $user = $this->manager->getRepository(User::class)->findOneBy(["id" => 2]);
        $crawler = $this->client->request('GET', '/connexion');

        $form = $crawler->selectButton('Se connecter')->form([
            'email' => $user->getEmail(),
            'password' => 'password'
        ]);

        $this->client->submit($form);
        $this->assertResponseRedirects('/');

        $crawler = $this->client->request('GET', '/mon-compte/supprimer-confirmation');
        $form = $crawler->selectButton('Supprimer mon compte')->form([
            'delete_account[password]' => 'password',
        ]);

        $this->client->submit($form);
        $this->assertResponseStatusCodeSame(302);
    }
}