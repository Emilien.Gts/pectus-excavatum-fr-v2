<?php

namespace App\Tests\Functional;

use App\Entity\User;

class ModifyMyInformationsTest extends FunctionalTest
{
    /**
     * Undocumented function
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = $this->client->getContainer()->get("doctrine.orm.entity_manager");
    }

    /**
     * Check good HTTP response, h1 and title
     *
     * @return void
     */
    public function testMyInformationsPageIsGood(): void
    {
        $this->userLogin($this->client);
        $this->pageIsGood(
            '/mon-compte/modifier-informations',
            'Modifier mes informations',
            'Modifier mes informations - Pectus Excavatum FR',
            $this->client
        );
    }

    /**
     * Test if the modify informations's form works
     *
     * @return void
     */
    public function testModifyInformationsFormWorks(): void
    {
        $this->userLogin($this->client);

        $crawler = $this->client->request('GET', '/mon-compte/modifier-informations');
        $form = $crawler->selectButton('Modifier mes informations')->form([
            'modify_informations[firstName]' => 'EEmmiilliieenn',
            'modify_informations[lastName]' => 'GGaannttooiiss',
            'modify_informations[pseudo]' => 'EEmmiilliieennGGttss',
            'modify_informations[plainPassword]' => 'password',
        ]);

        $this->client->submit($form);
        $this->assertResponseStatusCodeSame(200);

        $this->assertSelectorTextContains(
            "div.alert.alert__success",
            "Vos informations ont bien été modifiées."
        );
    }

    /**
     * Test if modify informations's form if password not good
     *
     * @return void
     */
    public function testModifyInformationsFormIfPasswordNotGood(): void
    {
        $this->userLogin($this->client);

        $crawler = $this->client->request('GET', '/mon-compte/modifier-informations');
        $form = $crawler->selectButton('Modifier mes informations')->form([
            'modify_informations[firstName]' => 'EEmmiilliieenn',
            'modify_informations[lastName]' => 'GGaannttooiiss',
            'modify_informations[pseudo]' => 'EEmmiilliieennGGttss',
            'modify_informations[plainPassword]' => 'password2',
        ]);

        $this->client->submit($form);
        $this->assertResponseStatusCodeSame(200);

        $this->assertSelectorTextContains(
            "div.alert.alert__danger",
            "Le mot de passe renseigné ne correspond pas à votre mot de passe actuel."
        );
    }
}
