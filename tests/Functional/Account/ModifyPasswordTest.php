<?php

namespace App\Tests\Functional;

class ModifyPasswordTest extends FunctionalTest
{
    /**
     * Undocumented function
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = $this->client->getContainer()->get("doctrine.orm.entity_manager");
    }

    /**
     * Check good HTTP response, h1 and title
     *
     * @return void
     */
    public function testMyInformationsPageIsGood(): void
    {
        $this->userLogin($this->client);
        $this->pageIsGood(
            '/mon-compte/modifier-mot-de-passe',
            'Modifier mon mot de passe',
            'Modifier mon mot de passe - Pectus Excavatum FR',
            $this->client
        );
    }

    /**
     * Test if the password's form works
     *
     * @return void
     */
    public function testModifyPasswordFormWorks(): void
    {
        $this->userLogin($this->client);

        $crawler = $this->client->request('GET', '/mon-compte/modifier-mot-de-passe');
        $form = $crawler->selectButton('Modifier mon mot de passe')->form([
            'modify_password[plainPassword]' => 'password',
            'modify_password[repeatedPassword]' => 'password',
            'modify_password[actualPassword]' => 'password',
        ]);

        $this->client->submit($form);
        $this->assertResponseStatusCodeSame(200);

        $this->assertSelectorTextContains(
            "div.alert.alert__success",
            "Votre mot de passe a bien été modifié."
        );
    }

    /**
     * Test password's form if password not good
     *
     * @return void
     */
    public function testModifyInformationsFormIfPasswordNotGood(): void
    {
        $this->userLogin($this->client);

        $crawler = $this->client->request('GET', '/mon-compte/modifier-mot-de-passe');
        $form = $crawler->selectButton('Modifier mon mot de passe')->form([
            'modify_password[plainPassword]' => 'password',
            'modify_password[repeatedPassword]' => 'password',
            'modify_password[actualPassword]' => 'password2',
        ]);

        $this->client->submit($form);
        $this->assertResponseStatusCodeSame(200);

        $this->assertSelectorTextContains(
            "div.alert.alert__danger",
            "Le mot de passe renseigné ne correspond pas à votre mot de passe actuel."
        );
    }

    /**
     * Test password's form if repeated password not good
     *
     * @return void
     */
    public function testModifyInformationsFormIfRepeatedPasswordNotGood(): void
    {
        $this->userLogin($this->client);

        $crawler = $this->client->request('GET', '/mon-compte/modifier-mot-de-passe');
        $form = $crawler->selectButton('Modifier mon mot de passe')->form([
            'modify_password[plainPassword]' => 'password',
            'modify_password[repeatedPassword]' => 'password2',
            'modify_password[actualPassword]' => 'password',
        ]);

        $this->client->submit($form);
        $this->assertResponseStatusCodeSame(200);

        $this->assertSelectorTextContains(
            "div.alert.alert__danger",
            "Votre nouveau mot de passe est différent de la confirmation de ce dernier."
        );
    }
}
