<?php

namespace App\Tests\Functional;

class MyInformationsTest extends FunctionalTest
{
    /**
     * Undocumented function
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    /**
     * Check good HTTP response, h1 and title
     *
     * @return void
     */
    public function testMyInformationsPageIsGood(): void
    {
        $this->userLogin($this->client);
        $this->pageIsGood(
            '/mon-compte',
            'Mes informations',
            'Mes informations - Pectus Excavatum FR',
            $this->client
        );
    }
}
