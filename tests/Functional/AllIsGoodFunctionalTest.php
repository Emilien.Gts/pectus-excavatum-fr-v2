<?php

namespace App\Tests\Functional;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AllIsGoodFunctionTest extends WebTestCase
{
    /**
     * Check if all is good for functional tests
     *
     * @return void
     */
    public function testAllIsGood(): void
    {
        $client = static::createClient();
        $client->request('GET', '/');

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }
}
