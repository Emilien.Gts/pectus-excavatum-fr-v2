<?php

namespace Tests\Functional\Authentication;

use App\Tests\Functional\FunctionalTest;
use Symfony\Component\HttpFoundation\Response;

class LoginPageTest extends FunctionalTest
{
    /**
     * Undocumented function
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    /**
     * Check if page exists and if page contains H1
     *
     * @return void
     */
    public function testLoginPageIsGood(): void
    {
        $this->pageIsGood(
            '/connexion',
            'Connexion',
            'Connexion - Pectus Excavatum FR',
            $this->client
        );
    }

    /**
     * Check successfull login
     *
     * @return void
     */
    public function testSuccessfullLogin(): void
    {
        $this->userLogin($this->client);
    }

    /**
     * Check login with bad credentials
     *
     * @return void
     */
    public function testLoginWithBadCredentials(): void
    {
        $crawler = $this->client->request('GET', '/connexion');
        $form = $crawler->selectButton('Se connecter')->form([
            'email' => 'john@doe.fr',
            'password' => 'fakepassword'
        ]);

        $this->client->submit($form);
        $this->assertResponseRedirects('/connexion');

        $this->client->followRedirect();
        $this->assertSelectorExists('.alert.alert__danger');
    }

    /**
     * Check successful logout
     *
     * @return void
     */
    public function testSuccessfullLogout(): void
    {
        $crawler = $this->client->request('GET', '/connexion');
        $form = $crawler->selectButton('Se connecter')->form([
            'email' => 'emilien@pectusexcavatum.fr',
            'password' => 'password'
        ]);

        $this->client->submit($form);
        $this->assertResponseRedirects('/');

        $this->client->request('GET', '/deconnexion');
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
        
        $response = $this->client->followRedirect();
        $this->assertTrue(str_contains($response->getUri(), '/connexion'));
    }

    /**
     * Check go to logout without be login
     *
     * @return void
     */
    public function testTryToLogoutWithoutBeLogin(): void
    {
        $this->client->request('GET', '/deconnexion');

        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
        
        $response = $this->client->followRedirect();
        $this->assertTrue(str_contains($response->getUri(), '/connexion'));
    }
}
