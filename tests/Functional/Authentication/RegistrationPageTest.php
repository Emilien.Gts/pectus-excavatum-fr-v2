<?php

namespace Tests\Functional\Authentication;

use App\Entity\User;
use App\Tests\Functional\FunctionalTest;

class RegistrationPageTest extends FunctionalTest
{
    /**
     * Undocumented function
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = $this->client->getContainer()->get("doctrine.orm.entity_manager");
    }

    /**
     * Check if page exists and if page contains H1
     *
     * @return void
     */
    public function testRegisterPageIsGood(): void
    {
        $this->pageIsGood(
            '/inscription',
            'Inscription',
            'Inscription - Pectus Excavatum FR',
            $this->client
        );
    }

    /**
     * Check successfull registration
     *
     * @return void
     */
    public function testSuccessfullRegistration(): void
    {
        $crawler = $this->client->request('GET', '/inscription');
        $form = $crawler->selectButton('S\'inscrire')->form([
            'registration[firstName]' => 'Emilien',
            'registration[lastName]' => 'Gantois',
            'registration[pseudo]' => 'EmilienGts',
            'registration[email]' => 'emilien+2@pectusexcavatum.fr',
            'registration[plainPassword][first]' => 'password',
            'registration[plainPassword][second]' => 'password'
        ]);

        $this->client->submit($form);
        $this->assertResponseRedirects('/connexion');

        $user = $this->manager->getRepository(User::class)->findOneBy([]);
        $this->assertTrue($user !== null);
    }

    /**
     * Check unsuccessfull registration
     *
     * @return void
     */
    public function testUnsuccessfullRegistration(): void
    {
        $crawler = $this->client->request('GET', '/inscription');
        $form = $crawler->selectButton('S\'inscrire')->form([
            'registration[firstName]' => 'E',
            'registration[lastName]' => 'Gantois',
            'registration[pseudo]' => 'EmilienGts',
            'registration[email]' => 'emilien+3@pectusexcavatum.fr',
            'registration[plainPassword][first]' => 'password',
            'registration[plainPassword][second]' => 'password'
        ]);

        $this->client->submit($form);

        $user = $this->manager->getRepository(User::class)->findOneBy(['email' => 'emilien+3@pectusexcavatum.fr']);
        $this->assertTrue($user === null);
    }
}
