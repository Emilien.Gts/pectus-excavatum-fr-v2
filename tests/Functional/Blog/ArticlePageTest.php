<?php

namespace Tests\Functional\Blog;

use App\Entity\Blog\Article;
use App\Tests\Functional\FunctionalTest;
use Symfony\Component\HttpFoundation\Response;

class ArticlePageTest extends FunctionalTest
{
    /**
     * Undocumented function
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = $this->client->getContainer()->get("doctrine.orm.entity_manager");
        $this->article = $this->manager->getRepository(Article::class)->findOneBy([]);
    }

    /**
     * Check if page exists and if page contains H1
     *
     * @return void
     */
    public function testArticlePageIsGood(): void
    {
        $this->pageIsGood(
            '/blog/' . $this->article->getSlug(),
            $this->article->getTitle(),
            $this->article->getTitle() . ' - Pectus Excavatum FR',
            $this->client
        );
    }

    /**
     * Check if the required components are here
     *
     * @return void
     */
    public function testComponentsAreHere(): void
    {
        $this->checkHowManyTimeComponentsAreHere(
            '/blog/' . $this->article->getSlug(),
            [
                ['selector' => 'h2.subtitle', 'howMany' => 1],
                ['selector' => '.article__thumbnail > img', 'howMany' => 1],
                ['selector' => '.article__content', 'howMany' => 1],
                ['selector' => '.article__comments', 'howMany' => 1],
                ['selector' => '.article__similar', 'howMany' => 1]
            ],
            $this->client
        );

        $crawler = $this->client->request('GET', '/blog/' . $this->article->getSlug());
        $cards = $crawler->filter('.article__similar .card');
        $this->assertTrue(count($cards) <= 3 && count($cards) >= 1);

        $this->client->request('GET', '/deconnexion');
        $this->checkHowManyTimeComponentsAreHere(
            '/blog/' . $this->article->getSlug(),
            [
                ['selector' => '.comment__new.connected > .alert.alert__info', 'howMany' => 0],
                ['selector' => '.comment__new.disconnected > .alert.alert__info', 'howMany' => 1]
            ],
            $this->client
        );

        $this->userLogin($this->client);
        $this->checkHowManyTimeComponentsAreHere(
            '/blog/' . $this->article->getSlug(),
            [
                ['selector' => '.comment__new.connected', 'howMany' => 1],
                ['selector' => '.comment__new.diconnected', 'howMany' => 0]
            ],
            $this->client
        );
        
        $this->client->request('GET', '/deconnexion');
        $this->checkHowManyTimeComponentsAreHere(
            '/blog/' . $this->article->getSlug(),
            [
                ['selector' => '.comment__new.connected > .alert.alert__info', 'howMany' => 0],
                ['selector' => '.comment__new.disconnected > .alert.alert__info', 'howMany' => 1]
            ],
            $this->client
        );

        $this->userLogin($this->client);
        $this->checkHowManyTimeComponentsAreHere(
            '/blog/' . $this->article->getSlug(),
            [
                ['selector' => '.comment__new.connected', 'howMany' => 1],
                ['selector' => '.comment__new.diconnected', 'howMany' => 0]
            ],
            $this->client
        );

        $this->client->request('GET', '/deconnexion');
    }

    /**
     * Check if the comments are here when article has comments
     *
     * @return void
     */
    public function testCommentsAreHereWhenArticleHaveComments(): void
    {
        $this->changeApprovalComments($this->article, true);
        $this->checkHowManyTimeComponentsAreHere(
            '/blog/' . $this->article->getSlug(),
            [
                ['selector' => '.comments.fill', 'howMany' => 1],
                ['selector' => '.comments.none > .alert.alert__info', 'howMany' => 0]
            ],
            $this->client
        );
    }

    /**
     * Check if the comments are not here when article hasn't comments
     *
     * @return void
     */
    public function testCommentsAreNotHereWhenArticleDoesNotHaveComments(): void
    {
        $this->changeApprovalComments($this->article, false);
        $this->checkHowManyTimeComponentsAreHere(
            '/blog/' . $this->article->getSlug(),
            [
                ['selector' => '.comments.none > .alert.alert__info', 'howMany' => 1],
                ['selector' => '.comments.fill', 'howMany' => 0]
            ],
            $this->client
        );
    }

    /**
     * Change the approval of the article's comments
     *
     * @param Article $article
     * @param boolean $isApproved
     * @return void
     */
    private function changeApprovalComments(Article $article, bool $isApproved): void
    {
        foreach ($article->getComments() as $comment) {
            $comment->setIsApproved($isApproved);
            $this->manager->persist($comment);
        }

        $this->manager->flush();
    }
}
