<?php

namespace Tests\Functional\Blog;

use App\Tests\Functional\FunctionalTest;

class BlogPageTest extends FunctionalTest
{
    /**
     * Undocumented function
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    /**
     * Check if page exists and if page contains H1
     *
     * @return void
     */
    public function testBlogPageIsGood(): void
    {
        $this->pageIsGood(
            '/blog',
            'Le blog du Pectus Excavatum',
            'Le blog du Pectus Excavatum - Pectus Excavatum FR',
            $this->client
        );
    }

    /**
     * Test if pagination working properly
     *
     * @return void
     */
    public function testPaginationAreWorking(): void
    {
        $crawler = $this->client->request('GET', '/blog');

        $link = $crawler->filter('span.page > a')->link()->getUri();
        $crawler = $this->client->request('GET', $link);

        $this->assertEquals($link, $crawler->getUri());

        $featuredPosts = $crawler->filter('.blog__featured-list > .card');
        $this->assertEquals(2, count($featuredPosts));

        $posts = $crawler->filter('.blog__list > .card');
        $this->assertTrue(count($posts) > 0);
        $this->assertTrue(count($posts) <= 6);
    }
}
