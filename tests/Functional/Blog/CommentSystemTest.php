<?php

namespace Tests\Functional\Blog;

use App\Entity\Blog\Article;
use App\Entity\Blog\ArticleComment;
use App\Tests\Functional\FunctionalTest;

class CommentSystemTest extends FunctionalTest
{
    /**
     * Undocumented function
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = $this->client->getContainer()->get("doctrine.orm.entity_manager");
        $this->article = $this->manager->getRepository(Article::class)->findOneBy([]);
    }

    /**
     * Test creation of a new comment
     *
     * @return void
     */
    public function testUserCreateANewComment()
    {
        $this->userLogin($this->client);

        $crawler = $this->client->request('GET', '/blog' . '/' . $this->article->getSlug());
        $form = $crawler->selectButton('Soumettre mon commentaire')->form([
            'comment[content]' => 'Commentaire de test',
        ]);

        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert.alert__success');

        $comments = $this->manager->getRepository(ArticleComment::class)->findBy(['article' => $this->article]);
        $this->assertEquals(3, count($comments));
    }
}
