<?php

namespace Tests\Functional\Blog;

use App\Tests\Functional\FunctionalTest;
use Symfony\Component\HttpFoundation\Response;

class ContactPageTest extends FunctionalTest
{
    /**
     * Undocumented function
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    /**
     * Check good HTTP response, h1 and title
     *
     * @return void
     */
    public function testHomePageIsGood(): void
    {
        $this->pageIsGood(
            '/contact',
            'Contact',
            'Contact - Pectus Excavatum FR',
            $this->client
        );
    }

    public function submitContactForm()
    {
        $crawler = $this->client->request('GET', '/contact');
        $form = $crawler->selectButton('Envoyer')->form([
            'contact[firstName]' => 'Emilien',
            'contact[lastName]' => 'Gantois',
            'contact[email]' => 'emilien@pectusexcavatum.fr',
            'contact[subject]' => 'Subject test',
            'contact[message]' => 'Message test',
        ]);

        $this->client->submit($form);
        $this->assertSelectorExists('.alert.alert__success');
    }
}
