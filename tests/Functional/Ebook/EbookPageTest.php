<?php

namespace Tests\Functional\Blog;

use App\Entity\Ebook\Ebook;
use App\Tests\Functional\FunctionalTest;

class EbookTestPage extends FunctionalTest
{
    /**
     * Undocumented function
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = $this->client->getContainer()->get("doctrine.orm.entity_manager");
        $this->ebook = $this->manager->getRepository(Ebook::class)->findOneBy([]);
    }

    /**
     * Check if page exists and if page contains H1
     *
     * @return void
     */
    public function testEbookPageIsGood(): void
    {
        $this->pageIsGood(
            '/ebook/' . $this->ebook->getSlug(),
            $this->ebook->getTitle(),
            $this->ebook->getTitle() . ' - Pectus Excavatum FR',
            $this->client
        );
    }

    /**
     * Check if the required components are here
     *
     * @return void
     */
    public function testComponentsAreHere(): void
    {
        $this->client->request('GET', '/deconnexion');
        $this->checkHowManyTimeComponentsAreHere(
            '/ebook/' . $this->ebook->getSlug(),
            [
                ['selector' => '.wide-card', 'howMany' => 1],
                ['selector' => '.steps', 'howMany' => 1],
                ['selector' => '.step'],
                ['selector' => '.opinions', 'howMany' => 1],
                ['selector' => '.opinion'],
                ['selector' => '.banner', 'howMany' => 2],
                ['selector' => 'form', 'howMany' => 1]
            ],
            $this->client
        );

        $this->userLogin($this->client);
        $this->checkHowManyTimeComponentsAreHere(
            '/ebook/' . $this->ebook->getSlug(),
            [
                ['selector' => '.ebook__download > a', 'howMany' => 1]
            ],
            $this->client
        );
    }
}
