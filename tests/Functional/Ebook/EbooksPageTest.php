<?php

namespace Tests\Functional\Ebook;

use App\Tests\Functional\FunctionalTest;

class EbooksPageTest extends FunctionalTest
{
    /**
     * Undocumented function
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    /**
     * Check if page exists and if page contains H1
     *
     * @return void
     */
    public function testEbooksPageIsGood(): void
    {
        $this->pageIsGood(
            '/ebook',
            'Ebooks',
            'Ebooks - Pectus Excavatum FR',
            $this->client
        );
    }

    /**
     * Test if components are here
     *
     * @return void
     */
    public function testComponentsAreHere(): void
    {
        $this->checkHowManyTimeComponentsAreHere(
            '/ebook',
            [
                ['selector' => '.ebooks__list', 'howMany' => 1],
                ['selector' => '.wide-card', 'howMany' => 4],
                ['selector' => '.pagination', 'howMany' => 1]
            ],
            $this->client
        );
    }

    /**
     * Test if pagination is working
     *
     * @return void
     */
    public function testPaginationAreWorking(): void
    {
        $crawler = $this->client->request('GET', '/ebook');

        $link = $crawler->filter('span.page > a')->link()->getUri();
        $crawler = $this->client->request('GET', $link);
        $this->assertEquals($link, $crawler->getUri());

        $cards = $crawler->filter('.wide-card');
        $this->assertTrue(count($cards) > 0);
        $this->assertTrue(count($cards) <= 4);
    }
}
