<?php

namespace Tests\Functional\Faq;

use App\Entity\Faq\Faq;
use App\Tests\Functional\FunctionalTest;

class FaqPageTest extends FunctionalTest
{
    /**
     * Undocumented function
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = $this->client->getContainer()->get("doctrine.orm.entity_manager");
        $this->faq = $this->manager->getRepository(Faq::class)->findOneBy([]);
    }

    /**
     * Check if page exists and if page contains H1
     *
     * @return void
     */
    public function testArticlePageIsGood(): void
    {
        $this->pageIsGood(
            '/faq/' . $this->faq->getSlug(),
            $this->faq->getQuestion(),
            'FAQ - ' . $this->faq->getQuestion() . ' - Pectus Excavatum FR',
            $this->client
        );
    }
}
