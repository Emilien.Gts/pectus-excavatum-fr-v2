<?php

namespace Tests\Functional\Faq;

use App\Entity\Faq\Faq;
use App\Tests\Functional\FunctionalTest;

class FaqsPageTest extends FunctionalTest
{
    /**
     * Undocumented function
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = $this->client->getContainer()->get("doctrine.orm.entity_manager");
        $this->faq = $this->manager->getRepository(Faq::class)->findOneBy([]);
    }

    /**
     * Check if page exists and if page contains H1
     *
     * @return void
     */
    public function testFaqsPageIsGood(): void
    {
        $this->pageIsGood(
            '/faq',
            'FAQ',
            'FAQ - Pectus Excavatum FR',
            $this->client
        );
    }

    /**
     * Test if search system is working
     *
     * @return void
     */
    public function testSearchIsWorking(): void
    {
        $value = explode(' ', trim($this->faq->getResponse()))[1];
        $crawler = $this->client->request('GET', '/faq');
        $form = $crawler->selectButton('Rechercher')->form([
            'search[search]' => $value,
        ]);

        $this->client->submit($form);
        $this->assertSelectorExists('.faq-search__request');
    }

    /**
     * Test if search system if no results
     *
     * @return void
     */
    public function testSearchIfNoResults(): void
    {
        $crawler = $this->client->request('GET', '/faq');
        $form = $crawler->selectButton('Rechercher')->form([
            'search[search]' => 'aaaa',
        ]);

        $this->client->submit($form);
        $this->assertSelectorExists('h2.subtitle');
    }
}
