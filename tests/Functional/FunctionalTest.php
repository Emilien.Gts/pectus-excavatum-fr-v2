<?php

namespace App\Tests\Functional;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

abstract class FunctionalTest extends WebTestCase
{
    /**
     * Check if the given page as a good HTTP response and contains H1 / title
     *
     * @param string $path
     * @param string $h1
     * @param string $title
     * @return void
     */
    public function pageIsGood(
        string $path,
        string $h1,
        string $title,
        Object $client
    ) {
        $client->request('GET', $path);

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertSelectorTextContains('h1', $h1);
        $this->assertSelectorTextContains('title', $title);
    }

    /**
     * Connect to the website
     *
     * @return void
     */
    public function userLogin(Object $client): void
    {
        $crawler = $client->request('GET', '/connexion');

        $form = $crawler->selectButton('Se connecter')->form([
            'email' => 'emilien@pectusexcavatum.fr',
            'password' => 'password'
        ]);

        $client->submit($form);
        $this->assertResponseRedirects('/');
    }

    /**
     * Check if a list of components is on the given page
     *
     * @param string $path
     * @param array $components
     * @return void
     */
    public function checkHowManyTimeComponentsAreHere(string $path, array $components, Object $client): void
    {
        $crawler = $client->request('GET', $path);

        foreach ($components as $component) {
            $this->checkComponent($crawler, $component);
        }
    }

    /**
     * Check how many time the component is on the given page
     *
     * @param Object $crawler
     * @param string $selector
     * @param string $howMany
     * @return void
     */
    public function checkComponent(Object $crawler, array $component): void
    {
        $element = $crawler->filter($component['selector']);
        array_key_exists('howMany', $component) ? 
            $this->assertEquals($component['howMany'], count($element)) : 
            $this->assertTrue($component['selector'] !== null);
    }
}
