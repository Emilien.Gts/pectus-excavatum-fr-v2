<?php

namespace Tests\Functional\Blog;

use App\Tests\Functional\FunctionalTest;
use Symfony\Component\HttpFoundation\Response;

class HomePageTest extends FunctionalTest
{
    /**
     * Undocumented function
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    /**
     * Check good HTTP response, h1 and title
     *
     * @return void
     */
    public function testHomePageIsGood(): void
    {
        $this->pageIsGood(
            '/',
            'Comprends et combats le Pectus Excavatum !',
            'Accueil - Pectus Excavatum FR',
            $this->client
        );
    }
}
