<?php

namespace Tests\Functional\Blog;

use App\Tests\Functional\FunctionalTest;

class RessourcesPage extends FunctionalTest
{
    /**
     * Undocumented function
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    /**
     * Check good HTTP response, h1 and title
     *
     * @return void
     */
    public function testRessourcesPageIsGood(): void
    {
        $this->pageIsGood(
            '/ressources',
            'Ressources',
            'Ressources - Pectus Excavatum FR',
            $this->client
        );
    }
}
