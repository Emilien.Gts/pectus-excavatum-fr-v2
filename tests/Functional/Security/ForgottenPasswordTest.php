<?php

namespace Tests\Functional\Secuiryt;

use App\Entity\User;
use App\Tests\Functional\FunctionalTest;

class ForgottenPasswordTest extends FunctionalTest
{
    /**
     * Undocumented function
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = $this->client->getContainer()->get("doctrine.orm.entity_manager");
        $this->user = $this->manager->getRepository(User::class)->findOneBy([]);
    }

    /**
     * Check if page exists and if page contains H1
     *
     * @return void
     */
    public function testLoginPageIsGood(): void
    {
        $this->pageIsGood(
            '/mot-de-passe-oublie',
            'Mot de passe oublié',
            'Mot de passe oublié - Pectus Excavatum FR',
            $this->client
        );
    }

    /**
     * Test forgotten password system
     *
     * @return void
     */
    public function testForgottenPasswordSystem(): void
    {
        $this->userLogin($this->client);

        $crawler = $this->client->request('GET', '/mot-de-passe-oublie');
        $form = $crawler->selectButton('Recevoir mon email')->form([
            'forgotten_password[email]' => $this->user->getEmail(),
        ]);

        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert.alert__success');
    }

    /**
     * Test reset password form
     *
     * @return void
     */
    public function testResetPassword(): void
    {
        $token = $this->manager->getRepository(User::class)->findOneBy([])->getForgottenPasswordToken();
        $crawler = $this->client->request('GET', '/reinitialiser-mot-de-passe/' . $token);
        $form = $crawler->selectButton('Réinitialiser mon mot de passe')->form([
            'reset_password[plainPassword]' => 'password',
        ]);

        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert.alert__success');
    }
}
