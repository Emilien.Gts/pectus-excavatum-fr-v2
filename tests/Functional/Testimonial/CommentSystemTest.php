<?php

namespace Tests\Functional\Testimonial;

use App\Entity\Testimonial\Testimonial;
use App\Entity\Testimonial\TestimonialComment;
use App\Tests\Functional\FunctionalTest;

class CommentSystemTest extends FunctionalTest
{
    /**
     * Undocumented function
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = $this->client->getContainer()->get("doctrine.orm.entity_manager");
        $this->testimonial = $this->manager->getRepository(Testimonial::class)->findOneBy([]);
    }

    /**
     * Test new comment is correctly created
     *
     * @return void
     */
    public function testUserCreateANewComment()
    {
        $this->userLogin($this->client);
        $crawler = $this->client->request('GET', '/temoignage' . '/' . $this->testimonial->getSlug());

        $form = $crawler->selectButton('Soumettre mon commentaire')->form([
            'comment[content]' => 'Commentaire de test',
        ]);
        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert.alert__success');

        $comments = $this->manager->getRepository(TestimonialComment::class)->findBy(['testimonial' => $this->testimonial]);
        $this->assertEquals(3, count($comments));
    }
}
