<?php

namespace Tests\Functional\Blog;

use App\Entity\Testimonial\Testimonial;
use App\Tests\Functional\FunctionalTest;

class TestimonialPageTest extends FunctionalTest
{
    /**
     * Undocumented function
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = $this->client->getContainer()->get("doctrine.orm.entity_manager");
        $this->testimonial = $this->manager->getRepository(Testimonial::class)->findOneBy([]);
    }

    /**
     * Check if page exists and if page contains H1
     *
     * @return void
     */
    public function testTestimonialPageIsGood(): void
    {
        $this->pageIsGood(
            '/temoignage/' . $this->testimonial->getSlug(),
            $this->testimonial->getTitle(),
            $this->testimonial->getTitle() . ' - Pectus Excavatum FR',
            $this->client
        );
    }

    /**
     * Check if the published date are display
     *
     * @return void
     */
    public function testComponentsAreHere(): void
    {
        $this->checkHowManyTimeComponentsAreHere(
            '/temoignage/' . $this->testimonial->getSlug(),
            [
                ['selector' => 'h2.subtitle'],
                ['selector' => 'testimonial__person'],
                ['selector' => 'testimonial__content'],
                ['selector' => 'testimonial__comments',],
                ['selector' => 'testimonial__similar'],
            ],
            $this->client
        );

        $crawler = $this->client->request('GET', '/temoignage' . '/' . $this->testimonial->getSlug());
        $cards = $crawler->filter('.card');
        $this->assertTrue(count($cards) <= 3 && count($cards) >= 1);

        $this->client->request('GET', '/deconnexion');
        $this->checkHowManyTimeComponentsAreHere(
            '/temoignage/' . $this->testimonial->getSlug(),
            [
                ['selector' => '.comment__new.disconnected', 'howMany' => 1],
                ['selector' => '.comment__new.connected', 'howMany' => 0],
            ],
            $this->client
        );

        $this->userLogin($this->client);
        $this->checkHowManyTimeComponentsAreHere(
            '/temoignage/' . $this->testimonial->getSlug(),
            [
                ['selector' => '.comment__new.connected', 'howMany' => 1],
                ['selector' => '.comment__new.disconnected', 'howMany' => 0],
            ],
            $this->client
        );

        $this->client->request('GET', '/deconnexion');
    }

    /**
     * Check if the comment area is here when user is login
     *
     * @return void
     */
    public function testCommentsAreHereWhenTetsimonialHasComments(): void
    {
        $this->changeApprovalComments($this->testimonial, true);
        $this->checkHowManyTimeComponentsAreHere(
            '/temoignage/' . $this->testimonial->getSlug(),
            [
                ['selector' => '.comments.fill', 'howMany' => 1],
                ['selector' => '.comments.none > .alert.alert__info', 'howMany' => 0]
            ],
            $this->client
        );
    }

    /**
     * Check if the comment area is not here when user is not login
     *
     * @return void
     */
    public function testCommentsAreNotHereWhenTestimonialDoesNotHaveComments(): void
    {
        $this->changeApprovalComments($this->testimonial, false);
        $this->checkHowManyTimeComponentsAreHere(
            '/temoignage/' . $this->testimonial->getSlug(),
            [
                ['selector' => '.comments.fill', 'howMany' => 0],
                ['selector' => '.comments.none > .alert.alert__info', 'howMany' => 1]
            ],
            $this->client
        );
    }

    /**
     * Change the approval of the testimonial's comments
     *
     * @param Testimonial $testimonial
     * @param boolean $isApproved
     * @return void
     */
    private function changeApprovalComments(Testimonial $testimonial, bool $isApproved): void
    {
        foreach ($testimonial->getComments() as $comment) {
            $comment->setIsApproved($isApproved);
            $this->manager->persist($comment);
        }

        $this->manager->flush();
    }
}
