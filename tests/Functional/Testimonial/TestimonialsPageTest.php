<?php

namespace Tests\Functional\Testimonial;

use App\Tests\Functional\FunctionalTest;

class TestimonialsPageTest extends FunctionalTest
{
    /**
     * Undocumented function
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    /**
     * Check if page exists and if page contains H1
     *
     * @return void
     */
    public function testBlogPageIsGood(): void
    {
        $this->pageIsGood(
            '/temoignage',
            'Témoignages',
            'Témoignages - Pectus Excavatum FR',
            $this->client
        );
    }

    /**
     * Test if required components are here
     *
     * @return void
     */
    public function testComponentsAreHere(): void
    {
        $this->checkHowManyTimeComponentsAreHere(
            '/temoignage',
            [
                ['selector' => '.pagination', 'howMany' => 1],

            ],
            $this->client
        );

        $crawler = $this->client->request('GET', '/temoignage');
        $posts = $crawler->filter('.testimonials__list > .card');
        $this->assertTrue(count($posts) > 0 && count($posts) <= 9);
    }

    /**
     * Test if pagination is working
     *
     * @return void
     */
    public function testPaginationAreWorking(): void
    {
        $crawler = $this->client->request('GET', '/temoignage');

        $link = $crawler->filter('span.page > a')->link()->getUri();
        $crawler = $this->client->request('GET', $link);

        $this->assertEquals($link, $crawler->getUri());

        $posts = $crawler->filter('.testimonials__list > .card');
        $this->assertTrue(count($posts) > 0);
        $this->assertTrue(count($posts) <= 9);
    }
}
