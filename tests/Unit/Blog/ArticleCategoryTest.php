<?php

namespace App\Tests\Unit\Blog;

use App\Entity\Blog\ArticleCategory;
use App\Tests\Unit\EntityTest;
use Faker\Factory;

class ArticleCategoryTest extends EntityTest
{
    /**
     * Return a correct entity
     *
     * @return ArticleCategory
     */
    public function getEntity(): ArticleCategory
    {
        return (new ArticleCategory())
            ->setName('La chirurgie de Nuss')
            ->setSlug('la-chirurgie-de-nuss');
    }

    /**
     * Check if the entity of getEntity() is correct
     *
     * @return void
     */
    public function testEntityIsValid(): void
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    /**
     * Check if the attribute title respects its constraints
     *
     * @return void
     */
    public function testTitleIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setName(''), 2);
        $this->assertHasErrors($this->getEntity()->setName('E'), 1);
        $this->assertHasErrors($this->getEntity()->setName(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute slug respects its constraints
     *
     * @return void
     */
    public function testSlugIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setSlug(implode(" ", $faker->words(100))), 1);
    }
}
