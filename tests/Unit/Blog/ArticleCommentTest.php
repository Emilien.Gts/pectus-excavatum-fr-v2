<?php

namespace App\Tests\Unit\Blog;

use Faker\Factory;
use App\Entity\User;
use App\Entity\Blog\Article;
use Doctrine\ORM\EntityManager;
use App\Repository\UserRepository;
use App\Entity\Blog\ArticleComment;
use App\Repository\Blog\ArticleRepository;
use App\Tests\Unit\EntityTest;

class ArticleCommentTest extends EntityTest
{
    private EntityManager $entityManager;

    private UserRepository $userRepository;

    private ArticleRepository $articleRepository;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->userRepository = $this->entityManager
            ->getRepository(User::class);

        $this->articleRepository = $this->entityManager
            ->getRepository(Article::class);
    }

    /**
     * Return a correct entity
     *
     * @return ArticleComment
     */
    public function getEntity(): ArticleComment
    {
        $article = $this->articleRepository->findOneBy([]);
        $user = $this->userRepository->findOneBy([]);

        return (new ArticleComment())
            ->setContent('Exemple de commentaire')
            ->setIsApproved(true)
            ->setResponse('Exemple de réponse')
            ->setCreatedAt(new \DateTimeImmutable())
            ->setArticle($article)
            ->setUser($user);
    }

    /**
     * Check if the entity of getEntity() is correct
     *
     * @return void
     */
    public function testEntityIsValid(): void
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    /**
     * Check if the attribute content respects its constraints
     *
     * @return void
     */
    public function testContentIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setContent(''), 2);
        $this->assertHasErrors($this->getEntity()->setContent(implode(" ", $faker->words(1000))), 1);
    }

    /**
     * Check if the attribute response respects its constraints
     *
     * @return void
     */
    public function testResponseIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setResponse(''), 1);
        $this->assertHasErrors($this->getEntity()->setResponse(implode(" ", $faker->words(1000))), 1);
    }
}
