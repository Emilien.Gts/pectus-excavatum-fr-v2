<?php

namespace App\Tests\Unit\Blog;

use Faker\Factory;
use App\Entity\Image;
use App\Entity\Blog\Article;
use App\Repository\ImageRepository;
use App\Tests\Unit\EntityTest;
use Doctrine\ORM\EntityManager;

class ArticleTest extends EntityTest
{
    private EntityManager $entityManager;

    private ImageRepository $imageRepository;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->imageRepository = $this->entityManager
            ->getRepository(Image::class);
    }

    /**
     * Return a correct entity
     *
     * @return Article
     */
    public function getEntity(): Article
    {
        return (new Article())
            ->setTitle('La chirurgie de Nuss, bilan après 3 ans')
            ->setSlug('la-chirurgie-de-nuss-bilan-apres-3-ans')
            ->setReadingTime(3)
            ->setContent('Bonjour à tous ! Aujourd\'hui, on va s\'intéresser au sujet que je conna...');
    }

    /**
     * Check if the entity of getEntity() is correct
     *
     * @return void
     */
    public function testEntityIsValid(): void
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    /**
     * Check if the attribute title respects its constraints
     *
     * @return void
     */
    public function testTitleIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setTitle(''), 2);
        $this->assertHasErrors($this->getEntity()->setTitle('E'), 1);
        $this->assertHasErrors($this->getEntity()->setTitle(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute slug respects its constraints
     *
     * @return void
     */
    public function testSlugIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setSlug(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute reading time respects its constraints
     *
     * @return void
     */
    public function testReadingTimeIsInvalid()
    {
        // $this->assertHasErrors($this->getEntity()->setReadingTime(-7), 1);
        $this->assertHasErrors($this->getEntity()->setReadingTime(0), 1);
    }

    /**
     * Check if the attribute content respects its constraints
     *
     * @return void
     */
    public function testContentIsInvalid()
    {
        $this->assertHasErrors($this->getEntity()->setContent(''), 1);
    }
}
