<?php

namespace App\Tests\Unit;

use Faker\Factory;
use App\Entity\Contact;

class ContactTest extends EntityTest
{
    /**
     * Return a correct entity
     *
     * @return Contact
     */
    public function getEntity(): Contact
    {
        return (new Contact())
            ->setFirstName('Emilien')
            ->setLastName('Gantois')
            ->setEmail('emilien+1@pectusexcavatum.fr')
            ->setSubject('password')
            ->setmessage('ojzopgfrjegjre');
    }

    /**
     * Check if the entity of getEntity() is correct
     *
     * @return void
     */
    public function testEntityIsValid(): void
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    /**
     * Check if the attribute firstName respects its constraints
     *
     * @return void
     */
    public function testFirstNameIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setFirstName(''), 2);
        $this->assertHasErrors($this->getEntity()->setFirstName('E'), 1);
        $this->assertHasErrors($this->getEntity()->setFirstName(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute lastName respects its constraints
     *
     * @return void
     */
    public function testLastNameIsInvalid(): void
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setLastName(''), 2);
        $this->assertHasErrors($this->getEntity()->setLastName('E'), 1);
        $this->assertHasErrors($this->getEntity()->setLastName(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute email respects its constraints
     *
     * @return void
     */
    public function testEmailIsInvalid()
    {
        $this->assertHasErrors($this->getEntity()->setEmail(''), 1);
        $this->assertHasErrors($this->getEntity()->setEmail('emilien@pectusexcavatumfr'), 1);
        $this->assertHasErrors($this->getEntity()->setEmail('emilienpectusexcavatum.fr'), 1);
    }

    /**
     * Check if the attribute subject respects its constraints
     *
     * @return void
     */
    public function testSubjectIsInvalid(): void
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setSubject(''), 2);
        $this->assertHasErrors($this->getEntity()->setSubject('E'), 1);
        $this->assertHasErrors($this->getEntity()->setSubject(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute message respects its constraints
     *
     * @return void
     */
    public function testMessageIsInvalid(): void
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setMessage(''), 1);
    }
}
