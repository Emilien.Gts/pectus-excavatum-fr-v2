<?php

namespace App\Tests\Unit\Ebook;

use Faker\Factory;
use App\Entity\Ebook\Ebook;
use App\Entity\Ebook\EbookChapter;
use App\Repository\Ebook\EbookRepository;
use App\Tests\Unit\EntityTest;
use Doctrine\ORM\EntityManager;

class EbookChapterTest extends EntityTest
{
    private EntityManager $entityManager;

    private EbookRepository $ebookRepository;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->ebookRepository = $this->entityManager
            ->getRepository(Ebook::class);
    }

    /**
     * Return a correct entity
     *
     * @return EbookChapter
     */
    public function getEntity(): EbookChapter
    {
        $ebook = $this->ebookRepository->findOneBy([]);

        $chapter = (new EbookChapter())->setTitle('Le chapitre en question')
            ->setContent('Le contenu du chapitre')
            ->setEbook($ebook);

        return $chapter;
    }

    /**
     * Check if the entity of getEntity() is correct
     *
     * @return void
     */
    public function testEntityIsValid(): void
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    /**
     * Check if the attribute title respects its constraints
     *
     * @return void
     */
    public function testTitleIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setTitle(''), 2);
        $this->assertHasErrors($this->getEntity()->setTitle('E'), 1);
        $this->assertHasErrors($this->getEntity()->setTitle(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute content respects its constraints
     *
     * @return void
     */
    public function testIntroductionIsInvalid()
    {
        $this->assertHasErrors($this->getEntity()->setContent(''), 1);
    }
}
