<?php

namespace App\Tests\Unit\Ebook;

use Faker\Factory;
use App\Entity\Image;
use App\Entity\Ebook\Ebook;
use Doctrine\ORM\EntityManager;
use App\Entity\Ebook\EbookOpinion;
use App\Repository\ImageRepository;
use App\Repository\Ebook\EbookRepository;
use App\Tests\Unit\EntityTest;

class EbookOpinionTest extends EntityTest
{
    private EntityManager $entityManager;

    private ImageRepository $imageRepository;

    private EbookRepository $ebookRepository;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->imageRepository = $this->entityManager
            ->getRepository(Image::class);

        $this->ebookRepository = $this->entityManager
            ->getRepository(Ebook::class);
    }

    /**
     * Return a correct entity
     *
     * @return EbookOpinion
     */
    public function getEntity(): EbookOpinion
    {
        $ebook = $this->ebookRepository->findOneBy([]);

        $opinion = (new EbookOpinion())->setFullName('Opinion test')
            ->setCity('Rennes')
            ->setOpinion('Bla ifjoiruzo jesfoizufoit')
            ->setEbook($ebook);

        return $opinion;
    }

    /**
     * Check if the entity of getEntity() is correct
     *
     * @return void
     */
    public function testEntityIsValid(): void
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    /**
     * Check if the attribute title respects its constraints
     *
     * @return void
     */
    public function testTitleIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setFullName(''), 2);
        $this->assertHasErrors($this->getEntity()->setFullName('E'), 1);
        $this->assertHasErrors($this->getEntity()->setFullName(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute city respects its constraints
     *
     * @return void
     */
    public function testCityIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setCity(''), 2);
        $this->assertHasErrors($this->getEntity()->setCity('E'), 1);
        $this->assertHasErrors($this->getEntity()->setCity(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute opinion respects its constraints
     *
     * @return void
     */
    public function testOpinionIsInvalid()
    {
        $this->assertHasErrors($this->getEntity()->setOpinion(''), 2);
    }
}
