<?php

namespace App\Tests\Unit\Ebook;

use Faker\Factory;
use App\Entity\Image;
use App\Entity\Ebook\Ebook;
use App\Repository\ImageRepository;
use App\Tests\Unit\EntityTest;
use Doctrine\ORM\EntityManager;

class EbookTest extends EntityTest
{
    private EntityManager $entityManager;

    private ImageRepository $imageRepository;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->imageRepository = $this->entityManager
            ->getRepository(Image::class);
    }

    /**
     * Return a correct entity
     *
     * @return Ebook
     */
    public function getEntity(): Ebook
    {
        $image = $this->imageRepository->findOneBy([]);

        return (new Ebook())
            ->setTitle('Développer son mental')
            ->setSubtitle('Apprendre à corriger sa posture quotidiennement, et à la conserver')
            ->setSlug('developper-son-mental')
            ->setIntroduction('Bonjour à tous ! Aujourd\'hui, on va s\'intéresser au sujet que je conna...')
            ->setDescription('Bonjour à tous ! Afsdfdsfsdfujourd\'hui, on va s\'intéresser au sujet que je conna...')
            ->setIsPublished(true);
    }

    /**
     * Check if the entity of getEntity() is correct
     *
     * @return void
     */
    public function testEntityIsValid(): void
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    /**
     * Check if the attribute title respects its constraints
     *
     * @return void
     */
    public function testTitleIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setTitle(''), 2);
        $this->assertHasErrors($this->getEntity()->setTitle('E'), 1);
        $this->assertHasErrors($this->getEntity()->setTitle(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute subtitle respects its constraints
     *
     * @return void
     */
    public function testSubtitleIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setTitle(''), 2);
        $this->assertHasErrors($this->getEntity()->setTitle('E'), 1);
        $this->assertHasErrors($this->getEntity()->setTitle(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute slug respects its constraints
     *
     * @return void
     */
    public function testSlugIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setSlug(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute introduction respects its constraints
     *
     * @return void
     */
    public function testIntroductionIsInvalid()
    {
        $this->assertHasErrors($this->getEntity()->setIntroduction(''), 2);
    }

    /**
     * Check if the attribute description respects its constraints
     *
     * @return void
     */
    public function testDescriptionIsInvalid()
    {
        $this->assertHasErrors($this->getEntity()->setDescription(''), 2);
    }
}
