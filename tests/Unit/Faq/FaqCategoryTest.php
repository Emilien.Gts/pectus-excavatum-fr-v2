<?php

namespace App\Tests\Unit\Ebook;

use Faker\Factory;
use App\Entity\Faq\FaqCategory;
use App\Tests\Unit\EntityTest;

class FaqCategoryTest extends EntityTest
{
    /**
     * Return a correct entity
     *
     * @return FaqCategory
     */
    public function getEntity(): FaqCategory
    {
        return (new FaqCategory())
            ->setName('Développer son mental')
            ->setSlug('developper-son-mental');
    }

    /**
     * Check if the entity of getEntity() is correct
     *
     * @return void
     */
    public function testEntityIsValid(): void
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    /**
     * Check if the attribute title respects its constraints
     *
     * @return void
     */
    public function testQuestionIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setName(''), 2);
        $this->assertHasErrors($this->getEntity()->setName('E'), 1);
        $this->assertHasErrors($this->getEntity()->setName(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute slug respects its constraints
     *
     * @return void
     */
    public function testSlugIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setSlug(''), 2);
        $this->assertHasErrors($this->getEntity()->setSlug('E'), 1);
        $this->assertHasErrors($this->getEntity()->setSlug(implode(" ", $faker->words(100))), 1);
    }
}
