<?php

namespace App\Tests\Unit\Ebook;

use Faker\Factory;
use App\Entity\Faq\Faq;
use App\Tests\Unit\EntityTest;
use DateTimeImmutable;

class FaqTest extends EntityTest
{
    /**
     * Return a correct entity
     *
     * @return Faq
     */
    public function getEntity(): Faq
    {
        return (new Faq())
            ->setQuestion('Développer son mental')
            ->setSlug('developper-son-mental')
            ->setResponse('Bonjour à tous ! Aujourd\'hui, on va s\'intéresser au sujet que je conna...')
            ->setIsPublished(true)
            ->setPublishedAt(new DateTimeImmutable());
    }

    /**
     * Check if the entity of getEntity() is correct
     *
     * @return void
     */
    public function testEntityIsValid(): void
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    /**
     * Check if the attribute title respects its constraints
     *
     * @return void
     */
    public function testQuestionIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setQuestion(''), 2);
        $this->assertHasErrors($this->getEntity()->setQuestion('E'), 1);
        $this->assertHasErrors($this->getEntity()->setQuestion(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute slug respects its constraints
     *
     * @return void
     */
    public function testSlugIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setSlug(''), 2);
        $this->assertHasErrors($this->getEntity()->setSlug('E'), 1);
        $this->assertHasErrors($this->getEntity()->setSlug(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute introduction respects its constraints
     *
     * @return void
     */
    public function testIntroductionIsInvalid()
    {
        $this->assertHasErrors($this->getEntity()->setResponse(''), 1);
    }
}
