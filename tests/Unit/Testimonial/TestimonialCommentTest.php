<?php

namespace App\Tests\Unit\Testimonial;

use Faker\Factory;
use App\Entity\User;
use App\Entity\Testimonial\Testimonial;
use App\Entity\Testimonial\TestimonialComment;
use App\Repository\Testimonial\TestimonialRepository;
use App\Repository\UserRepository;
use App\Tests\Unit\EntityTest;
use Doctrine\ORM\EntityManager;

class TestimonialCommentTest extends EntityTest
{
    private EntityManager $entityManager;

    private UserRepository $userRepository;

    private TestimonialRepository $testimonialRepository;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->userRepository = $this->entityManager
            ->getRepository(User::class);

        $this->testimonialRepository = $this->entityManager
            ->getRepository(Testimonial::class);
    }

    /**
     * Return a correct entity
     *
     * @return TestimonialComment
     */
    public function getEntity(): TestimonialComment
    {
        /**
         * Setting up User
         */
        $user = $this->userRepository->findOneBy([]);
        $testimonial = $this->testimonialRepository->findOneBy([]);

        return (new TestimonialComment())
            ->setContent('Exemple de commentaire')
            ->setIsApproved(true)
            ->setResponse('Exemple de réponse')
            ->setCreatedAt(new \DateTime())
            ->setTestimonial($testimonial)
            ->setUser($user);
    }

    /**
     * Check if the entity of getEntity() is correct
     *
     * @return void
     */
    public function testEntityIsValid(): void
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    /**
     * Check if the attribute content respects its constraints
     *
     * @return void
     */
    public function testContentIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setContent(''), 2);
        $this->assertHasErrors($this->getEntity()->setContent(implode(" ", $faker->words(1000))), 1);
    }

    /**
     * Check if the attribute response respects its constraints
     *
     * @return void
     */
    public function testResponseIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setResponse(''), 1);
        $this->assertHasErrors($this->getEntity()->setResponse(implode(" ", $faker->words(1000))), 1);
    }
}
