<?php

namespace App\Tests\Unit\Testimonial;

use Faker\Factory;
use App\Entity\Image;
use App\Entity\Testimonial\Testimonial;
use App\Repository\ImageRepository;
use App\Tests\Unit\EntityTest;
use Doctrine\ORM\EntityManager;

class TestimonialTest extends EntityTest
{
    private EntityManager $entityManager;

    private ImageRepository $imageRepository;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->imageRepository = $this->entityManager
            ->getRepository(Image::class);
    }

    /**
     * Return a correct entity
     *
     * @return Ebook
     */
    public function getEntity(): Testimonial
    {
        $image = $this->imageRepository->findOneBy([]);

        return (new Testimonial())
            ->setTitle('Témoignage sur la chirurgue de Nuss')
            ->setSlug('temoignage-sur-la-chiruguie-de-nuss')
            ->setReadingTime(2)
            ->setFullName('GANTOIS Emilien')
            ->setIsMan(true)
            ->setCity('Rennes')
            ->setTestimonial('Bonjour à tous ! Aujourd\'hui, on va s\'intéresser au sujet que je conna...')
            ->setIsPublished(true)
            ->setThumbnail($image);
    }

    /**
     * Check if the entity of getEntity() is correct
     *
     * @return void
     */
    public function testEntityIsValid(): void
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    /**
     * Check if the attribute title respects its constraints
     *
     * @return void
     */
    public function testTitleIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setTitle(''), 2);
        $this->assertHasErrors($this->getEntity()->setTitle('E'), 1);
        $this->assertHasErrors($this->getEntity()->setTitle(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute slug respects its constraints
     *
     * @return void
     */
    public function testSlugIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setSlug(''), 2);
        $this->assertHasErrors($this->getEntity()->setSlug('F'), 1);
        $this->assertHasErrors($this->getEntity()->setSlug(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute full name respects its constraints
     *
     * @return void
     */
    public function testFullNameIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setFullName(''), 2);
        $this->assertHasErrors($this->getEntity()->setFullName('E'), 1);
        $this->assertHasErrors($this->getEntity()->setFullName(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute city respects its constraints
     *
     * @return void
     */
    public function testCityIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setCity(''), 2);
        $this->assertHasErrors($this->getEntity()->setCity('E'), 1);
        $this->assertHasErrors($this->getEntity()->setCity(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute introduction respects its constraints
     *
     * @return void
     */
    public function testTestimonialIsInvalid()
    {
        $this->assertHasErrors($this->getEntity()->setTestimonial(''), 2);
    }
}
