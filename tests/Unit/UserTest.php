<?php

namespace App\Tests\Unit;

use Faker\Factory;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserTest extends EntityTest
{
    /**
     * Return a correct entity
     *
     * @return User
     */
    public function getEntity(): User
    {
        return (new User())
            ->setFirstName('Emilien')
            ->setLastName('Gantois')
            ->setPseudo('EmilienGts')
            ->setEmail('emilien+1@pectusexcavatum.fr')
            ->setPassword('password');
    }

    /**
     * Check if the entity of getEntity() is correct
     *
     * @return void
     */
    public function testEntityIsValid(): void
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    /**
     * Check if the attribute firstName respects its constraints
     *
     * @return void
     */
    public function testFirstNameIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setFirstName(''), 2);
        $this->assertHasErrors($this->getEntity()->setFirstName('E'), 1);
        $this->assertHasErrors($this->getEntity()->setFirstName(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute lastName respects its constraints
     *
     * @return void
     */
    public function testLastNameIsInvalid(): void
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setLastName(''), 2);
        $this->assertHasErrors($this->getEntity()->setLastName('E'), 1);
        $this->assertHasErrors($this->getEntity()->setLastName(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute pseudo respects its constraints
     *
     * @return void
     */
    public function testPseudoIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setPseudo(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute email respects its constraints
     *
     * @return void
     */
    public function testEmailIsInvalid()
    {
        $this->assertHasErrors($this->getEntity()->setEmail(''), 1);
        $this->assertHasErrors($this->getEntity()->setEmail('emilien@pectusexcavatumfr'), 1);
        $this->assertHasErrors($this->getEntity()->setEmail('emilienpectusexcavatum.fr'), 1);
    }

    /**
     * Check if the attribute roles respects its constraints
     *
     * @return void
     */
    public function testRolesIsInvalid()
    {
        $this->assertHasErrors($this->getEntity()->setRoles([]), 1);
    }
}
